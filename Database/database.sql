CREATE TABLE Products(
	ProductId int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	ProductName varchar(50) NOT NULL,
	ProductPrice int NOT NULL,
	Sale int,
	CategoryName varchar(20) NOT NULL,
	SaleDate date,
	Description varchar(300),
	AdminId int,
	FOREIGN KEY (AdminId) REFERENCES Admins(AdminId)
);

CREATE TABLE Variants(
	ProductId int NOT NULL,
	FOREIGN KEY (ProductId) REFERENCES Products(ProductId) on delete cascade,	
	SKU varchar(20) NOT NULL PRIMARY KEY,
	Color varchar(10) NOT NULL,
	Size varchar(5),
	Quantity int NOT NULL,
	Image1 varchar(50),
	Image2 varchar(50)
);

CREATE TABLE Users(
	UserID int NOT NULL AUTO_INCREMENT PRIMARY KEY,	
	UserName varchar(50),
	NumberPhone varchar(11),
	Address varchar(100),
	Email varchar(100) NOT NULL,
	PassWord varchar(50) NOT NULL
);

CREATE TABLE Admins(
	AdminId int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	AdminName varchar(50),
	NumberPhone varchar(11),
	Sex varchar(5),
	Address varchar(100),
	Email varchar(100) NOT NULL,
	PassWord varchar(50) NOT NULL,
	Image varchar(50)
);

CREATE TABLE Orders(
	OrderId int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	OrderDate date,
	UserId int,
	FOREIGN KEY (UserId) REFERENCES Users(UserId),
	OrderStatus varchar(40),
	OrderPay varchar(40),
	OrderAdress varchar(50)
);
	
CREATE TABLE OrderDetails(
	OrderId int,
	FOREIGN KEY (OrderId) REFERENCES Orders(OrderId),
	OrderDetailId int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	SKU varchar(20),
	FOREIGN KEY (SKU) REFERENCES Variants(SKU),
	Quantity int NOT NULL,
	Price int NOT NULL,
	Sale int
);


DROP TRIGGER IF EXISTS deleteVariant;
DELIMITER &&
create trigger deleteVariant before delete
	on Products
	for each row
	begin
	delete from Variants where ProductId = old.ProductId;
	END;
	&&
DELIMITER ;

DROP TRIGGER IF EXISTS deleteOrder;
DELIMITER &&
create trigger deleteOrder before delete
	on Orders
	for each row
	begin
	delete from OrderDetails where OrderId = old.OrderId;
	END;
	&&
DELIMITER ;

DROP TRIGGER IF EXISTS updateQuantityProduct;
DELIMITER &&
create trigger updateQuantityProduct AFTER INSERT
	on OrderDetails
	for each row
	begin
	update Variants set Quantity = Quantity - new.Quantity where SKU = new.SKU;
	END;
	&&
DELIMITER ;

DELIMITER &&
DROP FUNCTION IF EXISTS tinhTongSanPham;
CREATE FUNCTION tinhTongSanPham (orderId int)
RETURNS INTEGER
BEGIN
	DECLARE tongSanPham INT;
	SELECT SUM(Quantity)
	FROM OrderDetails
	WHERE OrderDetails.OrderId = orderId
	INTO tongSanPham;
RETURN tongSanPham;
END;
&&
DELIMITER ;


DELIMITER &&
DROP FUNCTION IF EXISTS tinhTongTien;
CREATE FUNCTION tinhTongTien (orderId int)
RETURNS INTEGER
BEGIN
	DECLARE tongTien INT;
	SELECT SUM((Quantity*Price)- ROUND(Price*Sale/100)*Quantity )
	FROM OrderDetails
	WHERE OrderDetails.OrderId = orderId
	INTO tongTien;
RETURN tongTien;
END;
&&
DELIMITER ;


DELIMITER &&
DROP FUNCTION IF EXISTS tinhThanhTien;
CREATE FUNCTION tinhThanhTien (orderDetailId int)
RETURNS INTEGER
BEGIN
	DECLARE thanhTien INT;
	SELECT (Quantity*Price)- ROUND(Price*Sale/100)*Quantity
	FROM OrderDetails
	WHERE OrderDetails.OrderDetailId = orderDetailId
	INTO thanhTien;
RETURN thanhTien;
END;
&&
DELIMITER ;

