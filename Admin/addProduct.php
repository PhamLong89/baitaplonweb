<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
  adminAccess();
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $errors = array();
    print_r($_POST['tableProductVariant']);
    if(empty($_POST['productName'])){
      $errors[] = "productName";
    }else{
      $productName = $conn->real_escape_string(strip_tags($_POST['productName']));
      if(trim($productName) == ""){
        $errors[] =  "productName";
      }
    }
    if(isset($_POST['categoryName']) && filter_var($_POST['categoryName'], FILTER_SANITIZE_STRING)) {
      $categoryProduct = $_POST['categoryName'];
      if(trim($categoryProduct) == ""){
        $errors[] = "categoryName";
      }
    } else {
      $errors[] = "categoryName";
    }
    if(empty($_POST['productPrice'])){
      $errors[] = "productPrice";
    }
    else{
      if(!filter_var($_POST['productPrice'], FILTER_VALIDATE_INT)){
        $errors[] = "productPrice";
      }
      else{
        $productPrice = $_POST['productPrice'];
        if($productPrice < 0){
          $errors[] = "productPrice";
        }
      }
    }
    if(empty($_POST['productSale'])){
      $productSale = 0;
    }
    else{
      if(!filter_var($_POST['productSale'], FILTER_VALIDATE_INT)){
        $errors[] = "productSale";
      }
      else{
        $productSale = $_POST['productSale'];
        if($productSale >100 || $productSale < 0){
          $errors[] = "productSale";
        }
      }
    }
    if(empty($_POST['productDescription'])) {
      $productDescription = "";
    } else {
      $productDescription = $conn->real_escape_string($_POST['productDescription']);
    }
    if(empty($_POST['colorVariant'])){
      $errors[] = "variant";
    }
    else{
      $colors = $_POST['colorVariant'];
      $sizes = $_POST['sizeVariant'];
      $quantitys = $_POST['quantityVariant'];
      $SKUs = $_POST['SKUVariant'];
    }

    if(empty($errors)) {
      $stmt = $conn->prepare("INSERT INTO Products (ProductName, ProductPrice, Sale, CategoryName, SaleDate,  Description, AdminId) VALUES (?, ?, ?,?, ?, ?, ?)");
      $stmt->bind_param("siisssi", $productName, $productPrice, $productSale, $categoryProduct, date("Y-m-d"), $productDescription, $_SESSION['AdminId']);
      $stmt->execute();
      $stmt->store_result();
      if($stmt->affected_rows == 1) {
        $sql = "SELECT MAX(ProductId) FROM Products";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
          $product = $result->fetch_assoc();
          $productId = $product['MAX(ProductId)'];
          $allowed = array('image/jpeg', 'image/jpg', 'image/png', 'images/x-png');

          foreach ($colors as $key => $color) {
            $renamed1 = null;
            $renamed2 = null;
            //Kiểm tra và di chuyển ảnh 1
            if(in_array(strtolower($_FILES['product-image1']['type'][$key]), $allowed)) {
              $tmp = explode('.', $_FILES['product-image1']['name'][$key]);
              $ext = end($tmp);
              $renamed1 = uniqid(rand(), true).'.'."$ext";
              if(!move_uploaded_file($_FILES['product-image1']['tmp_name'][$key], "img/".$renamed1)) {
                  $renamed1 = null;
              }
            }
            else{
              $renamed1 == null;
            }
            //Kiểm tra và di chuyển ảnh 2
            if(in_array(strtolower($_FILES['product-image2']['type'][$key]), $allowed)) {
              $tmp2 = explode('.', $_FILES['product-image2']['name'][$key]);
              $ext2 = end($tmp2);
              $renamed2 = uniqid(rand(), true).'.'."$ext2";
              if(!move_uploaded_file($_FILES['product-image2']['tmp_name'][$key], "img/".$renamed2)) {
                  $renamed2 = null;
              }
            }
            else{
              $renamed2 == null;
            }

            $stmt = $conn->prepare("INSERT INTO Variants (ProductId, SKU, Color, Size, Quantity, Image1, Image2) VALUES (?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("isssiss", $productId, $SKUs[$key], $color, $sizes[$key], $quantitys[$key], $renamed1, $renamed2);
            $stmt->execute();
            $stmt->store_result();
          }
        }
        $message = "<p class='success'>Thêm mới sản phẩm thành công</p>";
        $success = true;
      }
      else{
        $message = "<p class='error2'>Thêm mới sản phẩm thất bại</p>";
      }
      $stmt->close();
      $conn->close();
    }
    
  }
?>
<?php include 'sidebarAdmin.php';?>
            <main class="page-content">
                <div class="container">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Thêm sản phẩm</h4>
                    <hr />
                    <?php  
                      if(isset($message)){
                        echo $message;
                      }
                    ?>
                    <form action="" method="post" class="" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="productName" class="font-weight-bold">Tên sản phẩm <span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('productName', $errors)){
                                echo "<p class='error'>Vui lòng điền tên sản phẩm</p>";
                              }
                            ?>
                            </label>
                            <input type="text" class="form-control" id="productName" name="productName"  placeholder="Tên sản phẩm" value="<?php if(isset($_POST['productName'])) echo strip_tags($_POST['productName']); ?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="categoryName" class="font-weight-bold">Loại sản phẩm <span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('categoryName', $errors)){
                                echo "<p class='error'>Vui lòng chọn loại sản phẩm</p>";
                              }
                            ?>
                            </label>
                            <select class="custom-select" id="categoryName" name="categoryName" required>
                                <option value="" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "") echo "selected='selected'" ?> >Chọn loại sản phẩm</option>
                                <option value="Áo nữ" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Áo nữ") echo "selected='selected'" ?> >Áo nữ</option>
                                <option value="Quần nữ" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Quần nữ") echo "selected='selected'" ?> >Quần nữ</option>
                                <option value="Váy nữ" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Váy nữ") echo "selected='selected'" ?> >Váy nữ</option>
                                <option value="Phụ kiện nữ" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Phụ kiện nữ") echo "selected='selected'" ?> >Phụ kiện nữ</option>
                                <option value="Áo nam" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Áo nam") echo "selected='selected'" ?> >Áo nam</option>
                                <option value="Quần nam" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Quần nam") echo "selected='selected'" ?> >Quần nam</option>
                                <option value="Phụ kiện nam" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Phụ kiện nam") echo "selected='selected'" ?> >Phụ kiện nam</option>
                                <option value="Áo bé gái" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Áo bé gái") echo "selected='selected'" ?> >Áo bé gái</option>
                                <option value="Quần bé gái" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Quần bé gái") echo "selected='selected'" ?> >Quần bé gái</option>
                                <option value="Váy bé gái" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Váy bé gái") echo "selected='selected'" ?> >Váy bé gái</option>
                                <option value="Phụ kiện bé gái" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Phụ kiện bé gái") echo "selected='selected'" ?> >Phụ kiện bé gái</option>
                                <option value="Áo bé trai" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Áo bé trai") echo "selected='selected'" ?> >Áo bé trai</option>
                                <option value="Quần bé trai" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Quần bé trai") echo "selected='selected'" ?> >Quần bé trai</option>
                                <option value="Phụ kiện bé trai" <?php if(isset($_POST['categoryName']) && $_POST['categoryName'] == "Phụ kiện bé trai") echo "selected='selected'" ?> >Phụ kiện bé trai</option>
                            </select>
                            <div class="invalid-feedback">Example invalid custom select feedback</div>
                        </div>
                        <div class="form-group">
                            <label for="productPrice" class="font-weight-bold">Giá bán <span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('productPrice', $errors)){
                                echo "<p class='error'>Vui lòng nhập gía sản phẩm</p>";
                              }
                            ?>
                            </label>
                            <input class="form-control" type="number" onkeypress="isInputNumber(event)" min="0" value="<?php if(isset($_POST['productPrice'])) echo strip_tags($_POST['productPrice']); ?>" id="productPrice" name="productPrice" required/>
                        </div>

                        <div class="form-group">
                          <label class="font-weight-bold" for="productDescription">Mô tả </label>
                          <textarea name="productDescription" class="form-control" rows="5" cols="50" tabindex="3" >
                            <?php if(isset($_POST['productDescription'])) echo htmlentities($_POST['productDescription'], ENT_COMPAT, 'UTF-8');
                            ?>                              
                            </textarea>
                        </div>  

                        <div class="form-group">
                            <label for="productSale" class="font-weight-bold">Giảm giá(%)
                            <?php
                              if(isset($errors) && in_array('productSale', $errors)){
                                echo "<p class='error'>Phần trăm giảm giá không hợp lệ</p>";
                              }
                            ?>
                            </label>
                            <input class="form-control d-inline-block" type="number" onkeypress="isInputNumber(event)" min="0" max="200" value="<?php if(isset($_POST['productSale'])) echo strip_tags($_POST['productSale']); ?>" id="productSale" name="productSale"/>
                        </div>

                        <label for="productVariant" class="font-weight-bold">Variant<span class="text-danger">*</span>
                        <?php
                          if(isset($errors) && in_array('variant', $errors)){
                            echo "<p class='error'>Vui lòng thêm ít nhất 1 biến thể của sản phẩm</p>";
                          }
                        ?>
                        </label>
                        <p id="errorVariant" class="text-danger"></p>
                        <div class="container-fluid border border-dark p-0">
                          <div class="row ">
                            <div class="col-md-3 col-12">
                              <label for="productColor" class="">Màu sắc <span class="text-danger">*</span></label>
                              <select class="custom-select" id="productColor">
                                  <option value="">Chọn màu</option>
                                  <option value="Đỏ">Đỏ</option>
                                  <option value="Đen">Đen</option>
                                  <option value="Trắng">Trắng</option>
                                  <option value="Vàng">Vàng</option>
                                  <option value="Cam">Cam</option>
                                  <option value="Hồng">Hồng</option>
                                  <option value="Tím">Tím</option>
                                  <option value="Nâu">Nâu</option>
                                  <option value="Xanh">Xanh</option>
                                  <option value="Xám">Xám</option>
                              </select>
                              <p></p>
                            </div>
                            <div class="col-md-3 col-12">
                              <label for="productSize" class="">Kích cỡ</label>
                              <select class="custom-select" id="productSize">
                                  <option value="">Chọn kích cỡ</option>
                                  <option value="S">S</option>
                                  <option value="M">M</option>
                                  <option value="L">L</option>
                                  <option value="XL">XL</option>
                                  <option value="2XL">2XL</option>
                                  <option value="3XL">3XL</option>
                                  <option value="28">28</option>
                                  <option value="29">29</option>
                                  <option value="30">30</option>
                                  <option value="31">31</option>
                                  <option value="32">32</option>
                                  <option value="33">33</option>
                              </select>
                            </div>
                            <div class="col-md-3 col-12">
                              <label for="productQuantity" class="">Số lượng <span class="text-danger">*</span></label>
                              <input class="form-control" type="number" onkeypress="isInputNumber(event)" min="0" value="1" id="productQuantity" />
                            </div>
                            <div class="col-md-3 col-12">
                              <label for="productSKU" class="">SKU <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="productSKU" aria-describedby="emailHelp" placeholder="SKU" />
                            </div>
                          </div>
                          
                          <button type="button" class="btn btn-dark addVariant mt-3" id="addVariant">Thêm variant</button>
                          <div class="row table m-0" style="overflow-x:auto;">
                            <table class="table my-5" id="tableProductVariant" name="tableProductVariant">
                              <thead>
                                  <tr>
                                      <th scope="col">Màu sắc</th>
                                      <th scope="col">Kích cỡ</th>
                                      <th scope="col">Số lượng</th>
                                      <th scope="col">SKU</th>
                                      <th scope="col">Ảnh 1</th>
                                      <th scope="col">Ảnh 2</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  
                              </tbody>
                            </table>  
                          </div>
                          
                        </div>

                        <button type="submit" class="btn btn-info mt-4">Lưu sản phẩm</button>
                        <button type="button" class="btn btn-dark mt-4"><a class="text-light" href="admin.php">Hủy</a></button>
                        <?php 
                          if(isset($success) && $success){
                            echo "<script type='text/javascript'>
                                    deleteForm();
                                </script>";
                          }
                        ?>
                    </form>
                    
                </div>
            </main>
            <!-- page-content" -->
        </div>
    </body>
</html>
