<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
	adminAccess();
	$sql = "SELECT OrderId, UserName, NumberPhone, OrderAdress, OrderDate, OrderStatus, OrderPay, tinhTongSanPham(OrderId), tinhTongTien(OrderId) FROM Orders INNER JOIN Users ON Orders.UserId = Users.UserId";
	$result = $conn->query($sql);
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Danh sách đơn hàng</h4>
                    <hr />
                    <?php if (isset($message)) {
                        echo $message;
                    } ?>
                    <div class="row table" >
	                	<table class="table mt-4" id="" name="tableOrders">
						    <thead>
						        <tr>
						            <th scope="col">Mã đơn hàng</th>
						            <th scope="col">Tên khách hàng</th>
						            <th scope="col">SĐT khách hàng</th>
						            <th scope="col">Địa chỉ</th>
						            <th scope="col">Ngày mua</th>
						            <th scope="col">Tổng sản phẩm</th>
						            <th scope="col">Tổng tiền</th>
						            <th scope="col">Trạng thái</th>
						            <th scope="col">Thanh toán</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    		if ($result->num_rows > 0) {
			  							while($row = $result->fetch_assoc()) {
			  								echo 
			  								"<tr>
			  									<td>{$row['OrderId']}</td>
			  									<td>{$row['UserName']}</td>
			  									<td>{$row['NumberPhone']}</td>
			  									<td>{$row['OrderAdress']}</td>
			  									<td>{$row['OrderDate']}</td>
			  									<td>{$row['tinhTongSanPham(OrderId)']}</td>
			  									<td>{$row['tinhTongTien(OrderId)']} đ</td>
			  									<td>{$row['OrderStatus']}</td>
			  									<td>{$row['OrderPay']}</td>
												<td><a class='' href='orderDetail.php?id={$row['OrderId']}'>Chi tiết</a></td>
			  								</tr>";
			  							}
			  						}
			  						$conn->close();
						    	?>
						    </tbody>
						</table>
					</div>
                </div>
            	<button type="button" class="btn btn-dark mt-4 ml-5"><a class="text-light" href="admin.php">Thoát</a></button>
            </main>
            
            <!-- page-content" -->
        </div>
    </body>
</html>