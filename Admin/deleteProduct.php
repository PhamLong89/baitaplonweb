<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
  adminAccess();
	if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
		$productId = $_GET['id'];
		$sql = "SELECT ProductName, ProductPrice, CategoryName, SaleDate FROM Products WHERE ProductId = ?";
		if($stmt = $conn->prepare($sql)) {
			$stmt->bind_param('i', $productId);
			$stmt->execute();			
			$result = $stmt->get_result();
			if($result->num_rows == 1){
				$product = $result->fetch_assoc();
				$sqlVariant = "SELECT Color, Size, Quantity, SKU, Image1, Image2 FROM Variants WHERE ProductId = ?";
				if($stmt2 = $conn->prepare($sqlVariant)) {
					$stmt2->bind_param('i', $productId);
					$stmt2->execute();			
					$result2 = $stmt2->get_result();
				}
			}
			else{
        redirect_to("Admin/viewProduct.php");
			}		
		}
		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			$sql = "DELETE FROM Products WHERE ProductId = ?";
			if($stmt = $conn->prepare($sql)) {
				$stmt->bind_param('i', $productId);
				$stmt->execute();
				if($stmt->affected_rows == 1) {
					echo"<script>
                alert('Xóa sản phẩm thành công');
                window.location='http://localhost/BaiTapLonWeb/Admin/viewProduct.php';
              </script>";
				} else {
					$message = "<p class='error'>Xóa sản phẩm thất bại</p>";
				}
				$stmt->close();
			}
			$conn->close();
		}
	}
	else{
    redirect_to("Admin/viewProduct.php");
	}
?>
<?php include 'sidebarAdmin.php';?>
			       <main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Xóa sản phẩm</h4>
                    <hr />
                    <?php  
                      if(isset($message)){
                        echo $message;
                      }
                    ?>
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="productName" class="font-weight-bold">Tên sản phẩm: <?php if(isset($product)) echo $product['ProductName']; ?> 
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="categoryName" class="font-weight-bold">Loại sản phẩm: <?php if(isset($product)) echo $product['CategoryName']; ?> 
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="productPrice" class="font-weight-bold">Giá bán: <?php if(isset($product)) echo $product['ProductPrice']; ?> 
                            </label>
                        </div>

                        <label for="productVariant" class="font-weight-bold">Variant</label>
                        <p id="errorVariant" class="text-danger"></p>
                        <div class="container-fluid border border-dark">
                          
                          <div class="row table">
                            <table class="table mt-4" id="tableProductVariant" name="tableProductVariant">
                              <thead>
                                  <tr>
                                      <th scope="col">Màu sắc</th>
                                      <th scope="col">Kích cỡ</th>
                                      <th scope="col">Số lượng</th>
                                      <th scope="col">SKU</th>
                                      <th scope="col">Ảnh 1</th>
                                      <th scope="col">Ảnh 2</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php
                                  	if($result2->num_rows > 0){
                  										while($row = $result2->fetch_assoc()) {
                  											echo 
                  											"<tr>
                  												<td>{$row['Color']}</td>
                  												<td>{$row['Size']}</td>
                  												<td>{$row['Quantity']}</td>
                  												<td>{$row['SKU']}</td>
                  												<td>{$row['Image1']}</td>
                  												<td>{$row['Image2']}</td>
                  											</tr>";
                  										}
                  									}
                                  ?>
                              </tbody>
                            </table>  
                          </div>
                          
                        </div>
                        <button type="submit" class="btn btn-info mt-4">Xóa sản phẩm</button>
                        <button type="button" class="btn btn-dark mt-4"><a class="text-light" href="viewProduct.php">Hủy</a></button>
                    </form>
                </div>
            </main>
            <!-- page-content" -->
        </div>
    </body>
</html>