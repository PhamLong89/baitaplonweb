<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
    adminAccess();
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        $errors = array();        
        if(isset($_POST['currentPassword']) && preg_match('/^\w{4,20}$/', trim($_POST['currentPassword']))) {
            $currentPassword = $conn->real_escape_string(trim($_POST['currentPassword']));               
            $sql = "SELECT AdminName FROM Admins WHERE AdminId = ? AND PassWord = ?";
	        if($stmt = $conn->prepare($sql)) {

	            $stmt->bind_param('is', $_SESSION['AdminId'], sha1($currentPassword));
	            $stmt->execute();           
	            $result = $stmt->get_result();

	            if($result->num_rows == 1){

	                if(isset($_POST['newPassword']) && preg_match('/^\w{4,20}$/', trim($_POST['newPassword']))) {

                        if($_POST['newPassword'] == $_POST['confirmPassword']) {
                            $newPassword = $conn->real_escape_string(trim($_POST['newPassword'])); 
                            $sql = "UPDATE Admins SET PassWord = ? WHERE AdminId = ? LIMIT 1";
                            if($upStmt = $conn->prepare($sql)) {
			                    $upStmt->bind_param('si', sha1($newPassword), $_SESSION['AdminId']);
			                    $upStmt->execute();
			                    if($upStmt->affected_rows == 1) {
			                        $message = "<p class='success'>Đổi mật khẩu thành công</p>";
			                    }
			                    else{
			                        $message = "<p class='error2'>Đổi mật khẩu thất bại</p>";
			                    }           
			                }                           
                        } else {
                            $message = "<p class='error2'>Mật khẩu không trùng khớp</p>";
                        }
	                    
                    } else {
                        $message = "<p class='error2'>Mật khẩu mới phải nhiều hơn 4 kí tự</p>";
                    }
	                    
	            } else {
	                $message = "<p class='error2'>Mật khẩu hiện tại không chính xác</p>";
	            }
	        }
        } else {
           	$message = "<p class='error2'>Mật khẩu hiện tại không chính xác</p>";
        }
    } // END main IF
?>
<?php include 'sidebarAdmin.php';?>

			<main class="page-content">
                <div class="container">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Đổi mật khẩu</h4>
                    <hr />
                    <?php if (isset($message)) {
                        echo $message;
                    } ?>
                    <form class="m-md-4" action="" method="post">
					<div class="form-group">
					    <label for="currentPassword" class="font-weight-bold">Mật khẩu hiện tại <span class="text-danger">*</span></label>
					    <input type="password" class="form-control" id="currentPassword" placeholder="Mật khẩu hiện tại" name="currentPassword"/>
					</div>
					<div class="form-group">
					    <label for="newPassword" class="font-weight-bold">Mật khẩu mới <span class="text-danger">*</span></label>
					    <input type="password" class="form-control" id="newPassword" placeholder="Mật khẩu mới" name="newPassword"/>
					</div>
					<div class="form-group">
					    <label for="confirmPassword" class="font-weight-bold">Nhập lại mật khẩu <span class="text-danger">*</span></label>
					    <input type="password" class="form-control" id="confirmPassword" placeholder="Nhập lại mật khẩu" name="confirmPassword"/>
					</div>

					<button type="submit" class="btn btn-info text-white my-3 mt-md-5">Đổi mật khẩu</button>
					<button type="button" class="btn btn-dark my-3 mt-md-5"><a class="text-light" href="admin.php">Hủy</a></button>
				</form>
                </div>
            </main>
            <!-- page-content" -->
        </div>
    </body>
</html>