<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
    adminAccess();
    if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
        $adminId = $_GET['id'];
        $sql = "SELECT AdminName, NumberPhone, Sex, Address, Email, Image FROM Admins WHERE AdminId = ?";
        if($stmt = $conn->prepare($sql)) {
            $stmt->bind_param('i', $adminId);
            $stmt->execute();           
            $result = $stmt->get_result();
            if($result->num_rows == 1){
                $admin = $result->fetch_assoc();
            }
            else{
                redirect_to("Admin/viewAdmin.php");
            }       
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $sql = "DELETE FROM Admins WHERE AdminId = ?";
            if($stmt = $conn->prepare($sql)) {
                $stmt->bind_param('i', $adminId);
                $stmt->execute();
                if($stmt->affected_rows == 1) {
                    echo"<script>
                            alert('Xóa tài khoản admin thành công');
                            window.location='http://localhost/BaiTapLonWeb/Admin/viewAdmin.php';
                        </script>";
                } else {
                    $message = "<p class='error2'>Xóa tài khoản admin thất bại</p>";
                }
                $stmt->close();
            }
            $conn->close();
        }
    }else{
        redirect_to("Admin/viewAdmin.php");
    }
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Xóa admin</h4>
                    <hr />
                    <?php  
                      if(isset($message)){
                        echo $message;
                      }
                    ?>
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Tên admin: <?php if(isset($admin)) echo $admin['AdminName']; ?>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Số điện thoại: <?php if(isset($admin)) echo $admin['NumberPhone']; ?>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Giới tính: <?php if(isset($admin)) echo $admin['Sex']; ?>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Địa chỉ: <?php if(isset($admin)) echo $admin['Address']; ?>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Email: <?php if(isset($admin)) echo $admin['Email']; ?>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Hình ảnh: </label>
                            <img src="img/<?php if(isset($admin)) echo $admin['Image']; ?>" alt="" class="avatar img-thumbnail">
                        </div>
                        
                        <button type="submit" class="btn btn-info mt-4">Xóa admin</button>
                        <button type="button" class="btn btn-dark mt-4"><a class="text-light" href="viewAdmin.php">Hủy</a></button>
                    </form>
                </div>
            </main>
            <!-- page-content" -->
        </div>
    </body>
</html>