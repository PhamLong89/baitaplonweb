<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
    adminAccess();
	if(isset($_GET['SKU']) && isset($_GET['pId']) && filter_var($_GET['SKU'], FILTER_SANITIZE_STRING) && filter_var($_GET['pId'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
		$SKU = $_GET['SKU'];
		$productId = $_GET['pId'];
		$sql = "SELECT Color, Size, Quantity, SKU, Image1, Image2 FROM Variants WHERE SKU = ? AND ProductId = ?";
		if($stmt = $conn->prepare($sql)) {
			$stmt->bind_param('si', $SKU, $productId);
			$stmt->execute();			
			$result = $stmt->get_result();
			if($result->num_rows == 1){
				$variant = $result->fetch_assoc();
			}
			else{
				$path = "Admin/editProduct.php?id=".$productId;
                redirect_to($path);
			}
			$stmt->close();
		}
	}
	else{
		$path = "Admin/editProduct.php?id=".$productId;
        redirect_to($path);
	}
?>
<?php
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
    	$errors = array();
    	if(empty($_POST['productQuantity'])){
	      	$errors[] = "productQuantity";
	    }
	    else{
	      	if(!filter_var($_POST['productQuantity'], FILTER_VALIDATE_INT)){
	        	$errors[] = "productQuantity";
	      	}
	      	else{
	        	$productQuantity = $_POST['productQuantity'];
                if($productQuantity < 0){
                    $errors[] = "productQuantity";
                }
	      	}
	    }
        $allowed = array('image/jpeg', 'image/jpg', 'image/png', 'images/x-png');
        $renamed1 = $variant['Image1'];
        if(isset($_FILES['product-image1'])) {
            if(in_array(strtolower($_FILES['product-image1']['type']), $allowed)) {
                $tmp = explode('.', $_FILES['product-image1']['name']);
                $ext = end($tmp);
                $renamed1 = uniqid(rand(), true).'.'."$ext";
                if(!move_uploaded_file($_FILES['product-image1']['tmp_name'], "img/".$renamed1)) {
                    $renamed1 = $variant['Image1'];
                }
            }
        }
        // Xoa file da duoc upload va ton tai trong thu muc tam
        if(isset($_FILES['product-image1']['tmp_name']) && is_file($_FILES['product-image1']['tmp_name']) && file_exists($_FILES['product-image1']['tmp_name'])) {
            unlink($_FILES['product-image1']['tmp_name']);
        }
        $renamed2 = $variant['Image2'];
        if(isset($_FILES['product-image2'])) {
            if(in_array(strtolower($_FILES['product-image2']['type']), $allowed)) {
                $tmp2 = explode('.', $_FILES['product-image2']['name']);
                $ext2 = end($tmp2);
                $renamed2 = uniqid(rand(), true).'.'."$ext2";
                if(!move_uploaded_file($_FILES['product-image2']['tmp_name'], "img/".$renamed2)) {
                    $renamed2 = $variant['Image2'];
                }
            }
        }
        // Xoa file da duoc upload va ton tai trong thu muc tam
        if(isset($_FILES['product-image2']['tmp_name']) && is_file($_FILES['product-image2']['tmp_name']) && file_exists($_FILES['product-image2']['tmp_name'])) {
            unlink($_FILES['product-image2']['tmp_name']);
        }

	    if(empty($errors)) {
	      	$sql2 = "UPDATE Variants SET Quantity = ?, Image1 = ?, Image2 = ? WHERE SKU = ? AND ProductId = ?";
	      	if($upStmt = $conn->prepare($sql2)) {
	      		$upStmt->bind_param('isssi', $productQuantity, $renamed1, $renamed2, $SKU, $productId);
	      		$upStmt->execute();
	      		if($upStmt->affected_rows == 1) {
                    $message = "<p class='success'>Sửa biến thể sản phẩm thành công</p>";
                }
                else{
                	$message = "<p class='error2'>Sửa biến thể sản phẩm thất bại</p>";
                }   		
	      	}
	      	$upStmt->close();
			$conn->close();
	    }   
    }
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger">Sửa biến thể sản phẩm</h4>
                    <hr />
                    <?php  
                      	if(isset($message)){
                        	echo $message;
                      	}
                    ?>
                    <form action="" method="POST" enctype="multipart/form-data">
                    	<div class="form-group">
                    		<label class="font-weight-bold">Màu sắc: <?php if(isset($variant)) echo $variant['Color'];?> </label>	
                    	</div>
                    	<div class="form-group">
                    		<label class="font-weight-bold">Kích thước: <?php if(isset($variant)) echo $variant['Size'];?> </label>	
                    	</div>
                    	<div class="form-group">
                    		<label class="font-weight-bold">SKU: <?php if(isset($variant)) echo $variant['SKU'];?> </label>	
                    	</div>
                    	<div class="form-group">
                            <label for="productPrice" class="font-weight-bold">Số lượng <span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('productQuantity', $errors)){
                                echo "<p class='error'>Vui lòng nhập số lượng sản phẩm</p>";
                              }
                            ?>
                            </label>
                            <input class="form-control" type="number" onkeypress="isInputNumber(event)" min="0" value="<?php if(isset($_POST['productQuantity'])) echo strip_tags($_POST['productQuantity']); elseif(isset($variant)) echo $variant['Quantity']; ?>" id="productQuantity" name="productQuantity"/ required>
                        </div>
                        <div class="row">
                        	<div class="col-md-6 col-12">
                        		<p class="m-1">Ảnh 1</p>
                              	<input type="file" name="product-image1" id="product-image1" onchange="readURL(this, $('#img-product1'))" />
                              	<img src="<?php if(isset($renamed1)) echo"img/{$renamed1}"; elseif(isset($variant)) echo"img/{$variant['Image1']}"; ?>" alt="" class="img-variant img-thumbnail" id="img-product1">
                        	</div>
                        	<div class="col-md-6 col-12">
                        		<p class="m-1">Ảnh 2</p>
                              	<input type="file" name="product-image2" id="product-image2" onchange="readURL(this, $('#img-product2'))"/>
                              	<img src="<?php if(isset($renamed2)) echo"img/{$renamed2}"; elseif(isset($variant)) echo"img/{$variant['Image2']}"; ?>" alt="" class="img-variant img-thumbnail" id="img-product2">
                        	</div>
                        </div>
                    	
                    	<button type="submit" class="btn btn-info mt-4">Sửa variant</button>
                        <button type="button" class="btn btn-dark mt-4"><a class="text-light" href="editProduct.php?id=<?php echo $productId;?>">Hủy</a></button>
                    </form>
                </div>
            </main>
        </div>
    </body>
</html>
