<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>

<?php
    adminAccess();
    if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
        $adminId = $_GET['id'];
    }
    elseif(isset($_SESSION['AdminId'])){
        $adminId = $_SESSION['AdminId'];
    }
    else{
        redirect_to("Admin/viewAdmin.php");
    }
    $sql = "SELECT AdminName, NumberPhone, Sex, Address, Email, Image FROM Admins WHERE AdminId = ?";
    if($stmt = $conn->prepare($sql)) {
        $stmt->bind_param('i', $adminId);
        $stmt->execute();           
        $result = $stmt->get_result();
        if($result->num_rows == 1){
            $admin = $result->fetch_assoc();
        }
        else{
            redirect_to("Admin/viewAdmin.php");
        }       
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $errors = array();
        if(empty($_POST['adminName'])){
            $errors[] = "adminName";
        }else{
            $adminName = $conn->real_escape_string(strip_tags($_POST['adminName']));
            if(trim($adminName) == ""){
                $errors[] =  "adminName";
            }
        }
    
        if(empty($_POST['phoneNumberAdmin'])){
            $phoneNumberAdmin = "";
        }
        else{
            $conn->real_escape_string(trim($_POST['phoneNumberAdmin']));
            if(preg_match('/^[0]{1}[0-9]{9}$/', trim($_POST['phoneNumberAdmin'])) || preg_match('/^[+]{1}[8]{1}[4]{1}[0-9]{9}$/', trim($_POST['phoneNumberAdmin']))) {
                $phoneNumberAdmin = $conn->real_escape_string(trim($_POST['phoneNumberAdmin']));
            } else {
                $errors[] = "phoneNumberAdmin";
            }
        }

        if(isset($_POST['sexAdmin']) && filter_var($_POST['sexAdmin'], FILTER_SANITIZE_STRING)) {
            $sexAdmin = $_POST['sexAdmin'];
            if(trim($sexAdmin) == ""){
                $errors[] = "sexAdmin";
            }
        } else {
            $errors[] = "sexAdmin";
        }

        if(empty($_POST['addressAdmin'])){
            $errors[] = "addressAdmin";
        }else{
            $addressAdmin = $conn->real_escape_string(strip_tags($_POST['addressAdmin']));
            if(trim($addressAdmin) == ""){
                $errors[] = "addressAdmin";
            }
        }
        $renamed = $admin['Image'];
        if(isset($_FILES['imgAdmin'])) {
            $allowed = array('image/jpeg', 'image/jpg', 'image/png', 'images/x-png');
            if(in_array(strtolower($_FILES['imgAdmin']['type']), $allowed)) {
                $tmp = explode('.', $_FILES['imgAdmin']['name']);
                $ext = end($tmp);
                $renamed = uniqid(rand(), true).'.'."$ext";
                if(!move_uploaded_file($_FILES['imgAdmin']['tmp_name'], "img/".$renamed)) {
                    $renamed = $admin['Image'];
                    $errors[] = "imgAdmin";
                }
            }
        }
        // Xoa file da duoc upload va ton tai trong thu muc tam
        if(isset($_FILES['imgAdmin']['tmp_name']) && is_file($_FILES['imgAdmin']['tmp_name']) && file_exists($_FILES['imgAdmin']['tmp_name'])) {
            unlink($_FILES['imgAdmin']['tmp_name']);
        }

        if(empty($errors)) {
            $sql = "UPDATE Admins SET AdminName = ?, NumberPhone = ?, Sex = ?, Address = ?, Image= ? WHERE AdminId = ?";
            if($upStmt = $conn->prepare($sql)) {
                $upStmt->bind_param('sssssi',$adminName, $phoneNumberAdmin, $sexAdmin, $addressAdmin, $renamed, $adminId);
                $upStmt->execute();
                if($upStmt->affected_rows == 1) {
                    $message = "<p class='success'>Sửa thông tin admin thành công</p>";
                }
                else{
                    $message = "<p class='error2'>Sửa thông tin admin thất bại</p>";
                }           
            }
            $upStmt->close();
            $conn->close(); 
        }        
    }
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Sửa thông tin admin</h4>
                    <hr />
                    <?php if (isset($message)) {
                        echo $message;
                    } ?>
                    <form action="" method="POST" enctype="multipart/form-data">
                    	<div class="form-group">
						    <label for="imgAdmin" class="font-weight-bold">Hình ảnh</label>
						    <input type="file" name="imgAdmin" id="imgAdmin" onchange="readURL(this, $('#avatarAdmin2'))" />
						    <img id="avatarAdmin2" src="<?php if(isset($renamed)) echo "img/{$renamed}"; else echo "img/{$admin['Image']}"; ?>" alt="" class="avatar img-thumbnail">
						</div>
                        <div class="form-group">
                            <label for="adminName" class="font-weight-bold">Tên admin <span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('adminName', $errors)){
                                echo "<p class='error'>Vui lòng điền tên admin</p>";
                              }
                            ?>
                            </label>
                            <input type="text" class="form-control" id="adminName" name="adminName"  placeholder="Tên admin" value="<?php if(isset($_POST['adminName'])) echo strip_tags($_POST['adminName']); elseif(isset($admin)) echo $admin['AdminName']; ?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="phoneNumberAdmin" class="font-weight-bold">Số điện thoại<span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('phoneNumberAdmin', $errors)){
                                echo "<p class='error'>Số điện thoại không đúng định dạng</p>";
                              }
                            ?>
                            </label>
                            <input type="tel" class="form-control" id="phoneNumberAdmin" name="phoneNumberAdmin" placeholder="Số điện thoại" value="<?php if(isset($_POST['phoneNumberAdmin'])) echo strip_tags($_POST['phoneNumberAdmin']); elseif(isset($admin)) echo $admin['NumberPhone'];?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="sexAdmin" class="font-weight-bold">Giới tính<span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('sexAdmin', $errors)){
                                echo "<p class='error'>Vui lòng chọn giới tính</p>";
                              }
                            ?>
                            </label>
                            <select class="custom-select" id="sexAdmin" name="sexAdmin" >
                                <option value="Nam" <?php if(isset($_POST['sexAdmin']) && $_POST['sexAdmin'] == "Nam") echo "selected='selected'"; elseif(isset($admin) && $admin['Sex'] == "Nam") echo "selected='selected'"; ?> >Nam</option>
                                <option value="Nữ" <?php if(isset($_POST['sexAdmin']) && $_POST['sexAdmin'] == "Nữ") echo "selected='selected'"; elseif(isset($admin) && $admin['Sex'] == "Nữ") echo "selected='selected'"; ?> >Nữ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="addressAdmin" class="font-weight-bold">Địa chỉ<span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('addressAdmin', $errors)){
                                echo "<p class='error'>Vui lòng nhập địa chỉ</p>";
                              }
                            ?>
                            </label>
                            <input type="text" class="form-control" id="addressAdmin" name="addressAdmin"  placeholder="Địa chỉ" value="<?php if(isset($_POST['addressAdmin'])) echo strip_tags($_POST['addressAdmin']); elseif(isset($admin)) echo $admin['Address']; ?>" required/>
                        </div>
                        <div class="form-group">
						    <label for="emailAdmin" class="font-weight-bold">Email <span class="text-danger">*</span></label>
						    <input type="email" class="form-control" id="emailAdmin" aria-describedby="emailHelp" placeholder="Email admin" readonly value="<?php if(isset($admin)) echo $admin['Email']; ?>" required/>
						</div>

                        <button type="submit" class="btn btn-info mt-4">Sửa thông tin admin</button>
                        <button type="button" class="btn btn-dark mt-4"><a class="text-light" href="">Hủy</a></button>
                    </form>
                </div>
            </main>
            <!-- page-content" -->
        </div>
    </body>
</html>