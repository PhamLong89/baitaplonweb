<?php 
  include('mysqliConnect.php');
  include("../Customer/function.php");
?>
<?php
	adminAccess();
	$sql = "SELECT ProductId, ProductName, ProductPrice, Sale, CategoryName, SaleDate, Description, Email FROM Products INNER JOIN Admins ON Products.AdminId = Admins.AdminId";
	$result = $conn->query($sql);
?>
<?php include('sidebarAdmin.php'); ?>
			<main class="page-content">
	            <div class="container-fluid">
	                <h4 class="text-uppercase text-danger font-weight-bold text-center">Danh sách sản phẩm</h4>
	                <hr />
	                <form action="searchProduct.php" method="get" class="input-group col-lg-4 p-0 pl-md-3 float-right" enctype="multipart/form-data">
	                    <input type="text" class="form-control" name="search" placeholder="Tìm kiếm..." />
	                    <div class="input-group-append">
	                        <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
	                    </div>
	                </form>
	                <div class="row table" style="overflow-x:auto;">
	                	<table class="table mt-4" id="" name="tableProduct">
						    <thead>
						        <tr>
						            <th scope="col">Mã sản phẩm</th>
						            <th scope="col">Tên sản phẩm</th>
						            <th scope="col">Loại sản phẩm</th>
						            <th scope="col">Giá bán</th>
						            <th scope="col">Giảm giá</th>
						            <th scope="col">Mô tả</th>
						            <th scope="col">Ngày bán</th>
						            <th scope="col">Admin thêm</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    		if ($result->num_rows > 0) {
			  							while($row = $result->fetch_assoc()) {
			  								echo 
			  								"<tr>
			  									<td>{$row['ProductId']}</td>
			  									<td>{$row['ProductName']}</td>
			  									<td>{$row['CategoryName']}</td>
			  									<td>{$row['ProductPrice']}</td>
			  									<td>{$row['Sale']}%</td>
			  									<td>{$row['Description']}</td>
			  									<td>{$row['SaleDate']}</td>
			  									<td>{$row['Email']}</td>
			  									<td><a class='' href='editProduct.php?id={$row['ProductId']}'>Sửa</a></td>
												<td><a class='' href='deleteProduct.php?id={$row['ProductId']}'>Xóa</a></td>
			  								</tr>";
			  							}
			  						}
			  						$conn->close();
						    	?>
						    </tbody>
						</table>

	                </div>
	            </div>
	            <button type="button" class="btn btn-dark mt-4 ml-5"><a class="text-light" href="admin.php">Thoát</a></button>
	        </main>
	    </div>
	</body>
</html>