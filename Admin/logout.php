<?php include 'mysqliConnect.php'; ?>
<?php include '../Customer/function.php'; ?>
<?php 
	if(isset($_SESSION['AdminId']) || isset($_SESSION['UserID'])) {
        unset($_SESSION['AdminId']);
        unset($_SESSION['UserID']);
    } 
    redirect_to("Customer/index.php");

?>