<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
	adminAccess();
	if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
		$orderId = $_GET['id'];
		$sql = "SELECT OrderDetails.SKU, Products.ProductName, Variants.Color, Variants.Size, OrderDetails.Quantity, OrderDetails.Price, OrderDetails.Sale, tinhThanhTien(OrderDetails.OrderDetailId) FROM OrderDetails INNER JOIN Variants ON OrderDetails.SKU = Variants.SKU INNER JOIN Products ON Variants.ProductId = Products.ProductId WHERE OrderId = ?";
	    if($stmt = $conn->prepare($sql)) {
	        $stmt->bind_param('i', $orderId);
	        $stmt->execute();           
	        $result = $stmt->get_result();
	    }
	}
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Chi tiết đơn hàng: <?php echo $orderId;?></h4>
                    <hr />
                    <?php if (isset($message)) {
                        echo $message;
                    } ?>
                    <div class="row table">
	                	<table class="table mt-4" id="" name="tableOrderDetail">
						    <thead>
						        <tr>
						        	<th scope="col">SKU</th>
						            <th scope="col">Tên sản phẩm</th>
						            <th scope="col">Màu sắc</th>
						            <th scope="col">Kích cỡ</th>
						            <th scope="col">Giá bán</th>
						            <th scope="col">Số lượng</th>
						            <th scope="col">Giảm giá</th>
						            <th scope="col">Thành tiền</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    		if ($result->num_rows > 0) {
			  							while($row = $result->fetch_assoc()) {
			  								echo 
			  								"<tr>
			  									<td>{$row['SKU']}</td>
			  									<td>{$row['ProductName']}</td>
			  									<td>{$row['Color']}</td>
			  									<td>{$row['Size']}</td>
			  									<td>{$row['Price']} đ</td>
			  									<td>{$row['Quantity']}</td>
			  									<td>{$row['Sale']} %</td>
			  									<td>{$row['tinhThanhTien(OrderDetails.OrderDetailId)']} đ</td>
			  								</tr>";
			  							}
			  						}
			  						$conn->close();
						    	?>		
						    </tbody>
						</table>
					</div>
                </div>
                <button type="button" class="btn btn-dark mt-4 ml-5"><a class="text-light" href="viewOrders.php">Thoát</a></button>
            </main>
            <!-- page-content" -->
        </div>
    </body>
</html>