<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
	adminAccess();
	if(isset($_GET['search']) && filter_var($_GET['search'], FILTER_SANITIZE_STRING)){
		$search = $conn->real_escape_string(trim($_GET['search']));
		$search2 = "%" . $search . "%";
		$sql = "SELECT ProductId, ProductName, ProductPrice, Sale, CategoryName, SaleDate, Description, Email FROM Products INNER JOIN Admins ON Products.AdminId = Admins.AdminId WHERE ProductName LIKE ?";
		$stmt = $conn->prepare($sql);
        $stmt->bind_param('s', $search2);
        $stmt->execute();           
        $result = $stmt->get_result();
	}
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Kết quả tìm kiếm sản phẩm: <?php echo $search;?></h4>
                    <hr />
                    <div class="row table" style="overflow-x:auto;">
	                	<table class="table mt-4" id="" name="tableProduct">
						    <thead>
						        <tr>
						            <th scope="col">Mã sản phẩm</th>
						            <th scope="col">Tên sản phẩm</th>
						            <th scope="col">Loại sản phẩm</th>
						            <th scope="col">Giá bán</th>
						            <th scope="col">Giảm giá</th>
						            <th scope="col">Mô tả</th>
						            <th scope="col">Ngày bán</th>
						            <th scope="col">Admin thêm</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    		if ($result->num_rows > 0) {
			  							while($row = $result->fetch_assoc()) {
			  								echo 
			  								"<tr>
			  									<td>{$row['ProductId']}</td>
			  									<td>{$row['ProductName']}</td>
			  									<td>{$row['CategoryName']}</td>
			  									<td>{$row['ProductPrice']}</td>
			  									<td>{$row['Sale']}%</td>
			  									<td>{$row['Description']}</td>
			  									<td>{$row['SaleDate']}</td>
			  									<td>{$row['Email']}</td>
			  									<td><a class='' href='editProduct.php?id={$row['ProductId']}'>Sửa</a></td>
												<td><a class='' href='deleteProduct.php?id={$row['ProductId']}'>Xóa</a></td>
			  								</tr>";
			  							}
			  						}
			  						$conn->close();
						    	?>
						    </tbody>
						</table>
	                </div>
                </div>
            </main>
 		</div>
	</body>
</html>