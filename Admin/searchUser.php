<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
	adminAccess();
	if(isset($_GET['search']) && filter_var($_GET['search'], FILTER_SANITIZE_STRING)){
		$search = $conn->real_escape_string(trim($_GET['search']));
		$search2 = "%" . $search . "%";
		$sql = "SELECT UserID, UserName, NumberPhone, Address, Email FROM Users WHERE UserName LIKE ?";
		$stmt = $conn->prepare($sql);
        $stmt->bind_param('s', $search2);
        $stmt->execute();           
        $result = $stmt->get_result();
	}
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Kết quả tìm kiếm khách hàng: <?php echo $search;?></h4>
                    <hr />
                    <div class="row table" style="overflow-x:auto;">
	                	<table class="table mt-4" id="" name="tableProduct">
						    <thead>
						        <tr>
						            <th scope="col">Mã khách hàng</th>
						            <th scope="col">Họ tên</th>
						            <th scope="col">Số điện thoại</th>
						            <th scope="col">Địa chỉ</th>
						            <th scope="col">Email</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    		if ($result->num_rows > 0) {
			  							while($row = $result->fetch_assoc()) {
			  								echo 
			  								"<tr>
			  									<td>{$row['UserID']}</td>
			  									<td>{$row['UserName']}</td>
			  									<td>{$row['NumberPhone']}</td>
			  									<td>{$row['Address']}</td>
			  									<td>{$row['Email']}</td>
			  								</tr>";
			  							}
			  						}
			  						$conn->close();
						    	?>
						    </tbody>
						</table>
	                </div>
                </div>
            </main>
 		</div>
	</body>
</html>