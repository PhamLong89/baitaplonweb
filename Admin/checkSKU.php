<?php 
  include('mysqliConnect.php');
?>
<?php
    if(isset($_GET['SKU']) && filter_var($_GET['SKU'], FILTER_SANITIZE_STRING)) {
        $SKU = $conn->real_escape_string(trim($_GET['SKU']));
        $sql = "SELECT SKU, ProductId FROM Variants WHERE SKU = ?";
        if($stmt = $conn->prepare($sql)) {
            $stmt->bind_param('s', $SKU);
            $stmt->execute();       
            $result = $stmt->get_result();
            if($result->num_rows > 0){
                echo "NO";
            }
            else{
                echo "YES";
            }       
        }
    }
?>