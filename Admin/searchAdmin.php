<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
	adminAccess();
	if(isset($_GET['search']) && filter_var($_GET['search'], FILTER_SANITIZE_STRING)){
		$search = $conn->real_escape_string(trim($_GET['search']));
		$search2 = "%" . $search . "%";
		$sql = "SELECT AdminId, AdminName, NumberPhone, Sex, Address, Email, Image FROM Admins WHERE AdminName LIKE ?";
		$stmt = $conn->prepare($sql);
        $stmt->bind_param('s', $search2);
        $stmt->execute();           
        $result = $stmt->get_result();
	}
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Kết quả tìm kiếm admin: <?php echo $search;?></h4>
                    <hr />
                    <div class="row table" style="overflow-x:auto;">
	                	<table class="table mt-4" id="" name="tableProduct">
						    <thead>
						        <tr>
						            <th scope="col">Mã tài khoản</th>
						            <th scope="col">Tên admin</th>
						            <th scope="col">Số điện thoại</th>
						            <th scope="col">Giới tính</th>
						            <th scope="col">Địa chỉ</th>
						            <th scope="col">Email</th>
						            <th scope="col">Hình ảnh</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    		if ($result->num_rows > 0) {
			  							while($row = $result->fetch_assoc()) {
			  								echo 
			  								"<tr>
			  									<td>{$row['AdminId']}</td>
			  									<td>{$row['AdminName']}</td>
			  									<td>{$row['NumberPhone']}</td>
			  									<td>{$row['Sex']}</td>
			  									<td>{$row['Address']}</td>
			  									<td>{$row['Email']}</td>
			  									<td><img class='img-admin' src=\"img/{$row['Image']}\" alt='' /></td>
			  									<td><a class='' href='editAdmin.php?id={$row['AdminId']}'>Sửa</a></td>
												<td><a class='' href='deleteAdmin.php?id={$row['AdminId']}'>Xóa</a></td>
			  								</tr>";
			  							}
			  						}
			  						$conn->close();
						    	?>
						    </tbody>
						</table>
	                </div>
                </div>
            </main>
 		</div>
	</body>
</html>