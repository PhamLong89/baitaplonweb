<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
    adminAccess();
	if(isset($_GET['SKU']) && isset($_GET['pId']) && filter_var($_GET['SKU'], FILTER_SANITIZE_STRING) && filter_var($_GET['pId'], FILTER_VALIDATE_INT, array('min_range' =>1))) {

		$SKU = $_GET['SKU'];
		$productId = $_GET['pId'];
		$sql = "SELECT Color, Size, Quantity, SKU, Image1, Image2 FROM Variants WHERE SKU = ? AND ProductId = ?";
		if($stmt = $conn->prepare($sql)) {
			$stmt->bind_param('si', $SKU, $productId);
			$stmt->execute();			
			$result = $stmt->get_result();
			if($result->num_rows == 1){
				$variant = $result->fetch_assoc();
			}
			else{
				$path = "Admin/editProduct.php?id=".$productId;
				redirect_to($path);
			}
			$stmt->close();
		}
		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			$sql = "DELETE FROM Variants WHERE SKU = ? AND ProductId = ?";
			if($stmt = $conn->prepare($sql)) {
				$stmt->bind_param('si', $SKU, $productId);
				$stmt->execute();
				if($stmt->affected_rows == 1) {
					echo"<script>
                            alert('Xóa biến thể sản phẩm thành công');
                            window.location='http://localhost/BaiTapLonWeb/Admin/editProduct.php?id={$productId}';
                        </script>";
				} else {
					$message = "<p class='error'>Xóa biến thể sản phẩm thất bại</p>";
				}
				$stmt->close();
			}
			$conn->close();
		}
	}
	else{
		$path = "Admin/editProduct.php?id=".$productId;
		redirect_to($path);
	}
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Xóa biến thể sản phẩm</h4>
                    <hr />
                    <?php  
                      	if(isset($message)){
                        	echo $message;
                      	}
                    ?>
                    <form action="" method="POST">
                    	<div class="form-group">
                    		<label class="font-weight-bold">Màu sắc: <?php if(isset($variant)) echo $variant['Color'];?> </label>	
                    	</div>
                    	<div class="form-group">
                    		<label class="font-weight-bold">Kích thước: <?php if(isset($variant)) echo $variant['Size'];?> </label>	
                    	</div>
                    	<div class="form-group">
                    		<label class="font-weight-bold">SKU: <?php if(isset($variant)) echo $variant['SKU'];?> </label>	
                    	</div>
                    	<div class="form-group">
                    		<label class="font-weight-bold">Số lượng: <?php if(isset($variant)) echo $variant['Quantity'];?> </label>	
                    	</div>
                    	<button type="submit" class="btn btn-info mt-4">Xóa variant</button>
                    	<button type="button" class="btn btn-dark mt-4"><a class="text-light" href="editProduct.php?id=<?php echo $productId;?>">Hủy</a></button>
                    </form>
                </div>
            </main>
        </div>
    </body>
</html>