<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
    adminAccess();
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $errors = array();
        if(empty($_POST['adminName'])){
        $errors[] = "adminName";
        }else{
            $adminName = $conn->real_escape_string(strip_tags($_POST['adminName']));
            if(trim($adminName) == ""){
                $errors[] =  "adminName";
            }
        }
    
        if(empty($_POST['phoneNumberAdmin'])){
            $phoneNumberAdmin = "";
        }
        else{
            $conn->real_escape_string(trim($_POST['phoneNumberAdmin']));
            if(preg_match('/^[0]{1}[0-9]{9}$/', trim($_POST['phoneNumberAdmin'])) || preg_match('/^[+]{1}[8]{1}[4]{1}[0-9]{9}$/', trim($_POST['phoneNumberAdmin']))) {
                $phoneNumberAdmin = $conn->real_escape_string(trim($_POST['phoneNumberAdmin']));
            }
            else {
                $errors[] = "phoneNumberAdmin";
            }
        }
        if(isset($_POST['sexAdmin']) && filter_var($_POST['sexAdmin'], FILTER_SANITIZE_STRING)) {
            $sexAdmin = $_POST['sexAdmin'];
            if(trim($sexAdmin) == ""){
                $errors[] = "sexAdmin";
            }
        } else {
            $errors[] = "sexAdmin";
        }

        if(empty($_POST['addressAdmin'])){
            $errors[] = "addressAdmin";
        }else{
            $addressAdmin = $conn->real_escape_string(strip_tags($_POST['addressAdmin']));
            if(trim($addressAdmin) == ""){
                $errors[] = "addressAdmin";
            }
        }

        if(empty($_POST['emailAdmin'])){
            $errors[] = 'emailAdmin';
        }
        else{
          if(preg_match('/^\w+@[a-zA-Z]{3,}\.com$/i', trim($_POST['emailAdmin']))) {
                $emailAdmin = $conn->real_escape_string(strip_tags($_POST['emailAdmin']));
            } else {
                $errors[] = "emailAdmin";
            }
        }
        if(preg_match('/^[\w\'.-]{4,20}$/', trim($_POST['passwordAdmin']))) {
            if($_POST['passwordAdmin'] == $_POST['confirmPasswordAdmin']) {
                $passwordAdmin = $conn->real_escape_string(trim($_POST['passwordAdmin']));
            } else {
                // Neu mat khau khong phu hop voi nhau
                $errors[] = "confirmPasswordAdmin";
            }
        } else {
            $errors[] = 'passwordAdmin';
        }

        if(isset($_FILES['imgAdmin'])) {
            $allowed = array('image/jpeg', 'image/jpg', 'image/png', 'images/x-png');
            if(in_array(strtolower($_FILES['imgAdmin']['type']), $allowed)) {
                $tmp = explode('.', $_FILES['imgAdmin']['name']);
                $ext = end($tmp);
                $renamed = uniqid(rand(), true).'.'."$ext";
                if(!move_uploaded_file($_FILES['imgAdmin']['tmp_name'], "img/".$renamed)) {
                    $renamed = null;
                    $errors[] = "imgAdmin";
                }
            }
        }
        else{
            $renamed = null;
        }
        // Xoa file da duoc upload va ton tai trong thu muc tam
        if(isset($_FILES['imgAdmin']['tmp_name']) && is_file($_FILES['imgAdmin']['tmp_name']) && file_exists($_FILES['imgAdmin']['tmp_name'])) {
            unlink($_FILES['imgAdmin']['tmp_name']);
        }

        if(empty($errors)) {
            $sql = "SELECT AdminId FROM Admins WHERE Email = ?";
            if($stmt = $conn->prepare($sql)) {
                $stmt->bind_param('s', $emailAdmin);
                $stmt->execute();           
                $result = $stmt->get_result();
                $stmt->close();
                if($result->num_rows == 0){
                    $stmt2 = $conn->prepare("INSERT INTO Admins (AdminName, NumberPhone, Sex, Address, Email,  PassWord, Image) VALUES (?, ?, ?, ?, ?, ?, ?)");
                    $stmt2->bind_param("sssssss", $adminName, $phoneNumberAdmin, $sexAdmin, $addressAdmin, $emailAdmin, sha1($passwordAdmin), $renamed);
                    $stmt2->execute();
                    $stmt2->store_result();
                    if($stmt2->affected_rows == 1) {
                        $message = "<p class='success'>Thêm mới tài khoản admin thành công</p>";
                        $success = true;
                    }
                    else{
                        $message = "<p class='error2'>Thêm mới tài khoản admin thất bại</p>";
                    }
                    $stmt2->close();
                    $conn->close();
                }
                else{
                    $message = "<p class='error2'>Email đã được sử dụng</p>";
                }       
            }          
        }        
    }
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Thêm admin</h4>
                    <hr />
                    <?php if (isset($message)) {
                        echo $message;
                    } ?>
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="adminName" class="font-weight-bold">Tên admin <span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('adminName', $errors)){
                                echo "<p class='error'>Vui lòng điền tên admin</p>";
                              }
                            ?>
                            </label>
                            <input type="text" class="form-control" id="adminName" name="adminName"  placeholder="Tên admin" value="<?php if(isset($_POST['adminName'])) echo strip_tags($_POST['adminName']); ?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="phoneNumberAdmin" class="font-weight-bold">Số điện thoại<span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('phoneNumberAdmin', $errors)){
                                echo "<p class='error'>Số điện thoại không đúng định dạng</p>";
                              }
                            ?>
                            </label>
                            <input type="tel" class="form-control" id="phoneNumberAdmin" name="phoneNumberAdmin" placeholder="Số điện thoại" value="<?php if(isset($_POST['phoneNumberAdmin'])) echo strip_tags($_POST['phoneNumberAdmin']); ?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="sexAdmin" class="font-weight-bold">Giới tính<span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('sexAdmin', $errors)){
                                echo "<p class='error'>Vui lòng chọn giới tính</p>";
                              }
                            ?>
                            </label>
                            <select class="custom-select" id="sexAdmin" name="sexAdmin" >
                                <option value="Nam" <?php if(isset($_POST['sexAdmin']) && $_POST['sexAdmin'] == "Nam") echo "selected='selected'" ?> >Nam</option>
                                <option value="Nữ" <?php if(isset($_POST['sexAdmin']) && $_POST['sexAdmin'] == "Nữ") echo "selected='selected'" ?> >Nữ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="addressAdmin" class="font-weight-bold">Địa chỉ<span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('addressAdmin', $errors)){
                                echo "<p class='error'>Vui lòng nhập địa chỉ</p>";
                              }
                            ?>
                            </label>
                            <input type="text" class="form-control" id="addressAdmin" name="addressAdmin"  placeholder="Địa chỉ" value="<?php if(isset($_POST['addressAdmin'])) echo strip_tags($_POST['addressAdmin']); ?>" required/>
                        </div>
                        <div class="form-group">
						    <label for="emailAdmin" class="font-weight-bold">Email <span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('emailAdmin', $errors)){
                                echo "<p class='error'>Địa chỉ email không hợp lệ</p>";
                              }
                            ?>
                            </label>
						    <input type="email" class="form-control" id="emailAdmin" aria-describedby="emailHelp" name="emailAdmin" placeholder="Email admin" value="<?php if(isset($_POST['emailAdmin'])) echo strip_tags($_POST['emailAdmin']); ?>" required/>
						</div>
						<div class="form-group">
						    <label for="passwordAdmin" class="font-weight-bold">Mật khẩu <span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('passwordAdmin', $errors)){
                                echo "<p class='error'>Mật khẩu nhiều hơn 4 kí tự</p>";
                              }
                            ?>
                            </label>
						    <input type="password" class="form-control" name="passwordAdmin" id="passwordAdmin" placeholder="Mật khẩu" value="<?php if(isset($_POST['passwordAdmin'])) echo strip_tags($_POST['passwordAdmin']); ?>" required/>
						</div>
						<div class="form-group">
						    <label for="confirmPasswordAdmin" class="font-weight-bold">Nhập lại mật khẩu <span class="text-danger">*</span>
                            <?php
                              if(isset($errors) && in_array('confirmPasswordAdmin', $errors)){
                                echo "<p class='error'>Mật khẩu không khớp</p>";
                              }
                            ?>
                            </label>
						    <input type="password" class="form-control" id="confirmPasswordAdmin" name="confirmPasswordAdmin" value="<?php if(isset($_POST['confirmPasswordAdmin'])) echo strip_tags($_POST['confirmPasswordAdmin']); ?>" placeholder="Nhập lại mật khẩu" required/>
						</div>
						<div class="form-group">
						    <label for="imgAdmin" class="font-weight-bold">Hình ảnh
                            <?php
                              if(isset($errors) && in_array('imgAdmin', $errors)){
                                echo "<p class='error'>Lỗi tải ảnh lên</p>";
                              }
                            ?>
                            </label>
						    <input type="file" name="imgAdmin" id="imgAdmin" onchange="readURL(this, $('#avatarAdmin'))" />
						    <img id="avatarAdmin" class="avatar" src="img/<?php if(isset($renamed))echo $renamed; else echo "no_avatar.jpg";?>" alt="Avatar admin" class="img-thumbnail" >
						</div>

                        <button type="submit" class="btn btn-info mt-4">Thêm admin</button>
                        <button type="button" id="huy" onclick="deleteForm()" class="btn btn-dark mt-4"><a class="text-light" href="admin.php">Hủy</a></button>
                        <?php 
                            if(isset($success) && $success){
                                echo "<script type='text/javascript'>
                                        deleteForm();
                                    </script>";
                            }
                        ?>
                    </form>
                </div>
            </main>
            <!-- page-content" -->
        </div>
        
    </body>
</html>