<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
	adminAccess();
	$sql = "SELECT UserID, UserName, NumberPhone, Address, Email FROM Users";
	$result = $conn->query($sql);
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Danh sách khách hàng</h4>
                    <hr />
                    <form action="searchUser.php" method="get" class="input-group col-lg-4 p-0 pl-md-3 float-right" enctype="multipart/form-data">
	                    <input type="text" class="form-control" name="search" placeholder="Tìm kiếm..." />
	                    <div class="input-group-append">
	                        <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
	                    </div>
	                </form>
                    <div class="row table" style="overflow-x:auto;">
	                	<table class="table mt-4" id="" name="tableProductVariant">
						    <thead>
						        <tr>
						            <th scope="col">Mã khách hàng</th>
						            <th scope="col">Họ tên</th>
						            <th scope="col">Số điện thoại</th>
						            <th scope="col">Địa chỉ</th>
						            <th scope="col">Email</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    		if ($result->num_rows > 0) {
			  							while($row = $result->fetch_assoc()) {
			  								echo 
			  								"<tr>
			  									<td>{$row['UserID']}</td>
			  									<td>{$row['UserName']}</td>
			  									<td>{$row['NumberPhone']}</td>
			  									<td>{$row['Address']}</td>
			  									<td>{$row['Email']}</td>
			  								</tr>";
			  							}
			  						}
			  						$conn->close();
						    	?>
						    </tbody>
						</table>
					</div>
                </div>
                <button type="button" class="btn btn-dark mt-4 ml-5"><a class="text-light" href="admin.php">Thoát</a></button>
            </main>
            <!-- page-content" -->
        </div>
    </body>
</html>