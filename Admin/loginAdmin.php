<?php
	ini_set('session.use_only_cookies', true);
	session_start();
?> 
<?php include 'mysqliConnect.php'; ?>
<?php include '../Customer/function.php'; ?>
<?php 
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
        $errors = array();
        if(empty($_POST['emailAdmin'])){
            $errors[] = 'emailAdmin';
        }
        else{
          if(preg_match('/^\w+@[a-zA-Z]{3,}\.com$/i', trim($_POST['emailAdmin']))) {
                $emailAdmin = $conn->real_escape_string(strip_tags($_POST['emailAdmin']));
            } else {
                $errors[] = "emailAdmin";
            }
        }      
        if(preg_match('/^[\w\'.-]{4,20}$/', trim($_POST['passwordAdmin']))) {
            $passwordAdmin = $conn->real_escape_string(trim($_POST['passwordAdmin']));
        } else {
            $errors[] = 'passwordAdmin';
        }
        if(empty($errors)) {

	        $sql = "SELECT AdminId, AdminName, Image FROM Admins WHERE Email = ? AND PassWord = ?";
	        if($stmt = $conn->prepare($sql)) {
	            $stmt->bind_param('ss', $emailAdmin, sha1($passwordAdmin));
	            $stmt->execute();           
	            $result = $stmt->get_result();
	            if($result->num_rows == 1){
	            	session_regenerate_id();
	                $admin = $result->fetch_assoc();
	                $_SESSION['AdminId'] = $admin['AdminId'];
	                $_SESSION['AdminName'] = $admin['AdminName'];
	                $_SESSION['Image'] = $admin['Image'];
	                $_SESSION['Email'] = $emailAdmin;
	                redirect_to("Admin/admin.php");
	            }
	            else{
	            	$message = "<p class='error2'>Tên tài khoản hoặc mật khẩu không chính xác</p>";
	            }       
	        }
        } else {
            $message = "<p class='error2'>Tên tài khoản hoặc mật khẩu không chính xác</p>";
        }
    
    } // END MAIN IF
?>
<html lang="en">
<head>
	<title>Đăng nhập admin</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="../Customer/style.css" />
    <link rel="stylesheet" type="text/css" href="style2.css" />
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <script src="../JS/jquery-3.3.1.min.js"></script>
    <script src="../JS/popper.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid ">
		<div class="row">
			<div class="col-md-6 text-uppercase text-center my-auto register" >
				Đăng nhập
			</div>
			<div class="col-md-6 bg-info p-md-4 pt-3">
				<?php  
                    if(isset($message)){
                    	echo $message;
                    }
                ?>
				<form class="m-md-4" action="" method="post">
					<div class="form-group">
					    <label for="emailAdmin" class="font-weight-bold">Email <span class="text-danger">*</span></label>
					    <input type="email" name="emailAdmin" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email" />
					</div>
					<div class="form-group">
					    <label for="passwordAdmin" class="font-weight-bold">Mật khẩu <span class="text-danger">*</span></label>
					    <input type="password" class="form-control" id="password" placeholder="Mật khẩu" name="passwordAdmin"/>
					</div>
					<button type="submit" class="btn btn-dark text-white my-3 mt-md-5">Đăng nhập</button>
				</form>
				<a class="font-weight-bold d-block ml-md-4 my-3" href="">Quên mật khẩu?</a>
			</div>
		</div>
	</div>
</body>
</html>