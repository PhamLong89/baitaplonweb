<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
    adminAccess();
  	if($_SERVER['REQUEST_METHOD'] == 'POST'){
  		$errors = array();
  		if(empty($_POST['Color'])){
      		$errors[] = "Color";
    	}
    	else{
      		$Color = $conn->real_escape_string(trim($_POST['Color']));
      		
    	}
    	if(empty($_POST['Size'])){
      		$Size = "";
    	}
    	else{
      		$Size = $conn->real_escape_string(trim($_POST['Size']));
      		
    	}
    	if(empty($_POST['Quantity'])){
      		$errors[] = "Quantity";
    	}
    	else{
      		if(!filter_var($_POST['Quantity'], FILTER_VALIDATE_INT)){
	        	$errors[] = "Quantity";
	      	}
	      	else{
		        $Quantity = $_POST['Quantity'];
		        if($Quantity < 0){
		          	$errors[] = "Quantity";
		        }
		    }      		
    	}
    	if(empty($_POST['SKU'])){
      		$errors[] = "SKU";
    	}
    	else{
      		$SKU = $conn->real_escape_string(trim($_POST['SKU']));   		
    	}
    	if(empty($_POST['productId'])){
      		$errors[] = "productId";
    	}
    	else{
      		if(!filter_var($_POST['productId'], FILTER_VALIDATE_INT)){
	        	$errors[] = "productId";
	      	}
	      	else{
		        $productId = $_POST['productId'];
		        if($productId < 0){
		          	$errors[] = "productId";
		        }
		    }      		
    	}
    	$allowed = array('image/jpeg', 'image/jpg', 'image/png', 'images/x-png');
    	if(isset($_FILES['product-image1'])) {
            if(in_array(strtolower($_FILES['product-image1']['type']), $allowed)) {
                $tmp = explode('.', $_FILES['product-image1']['name']);
                $ext = end($tmp);
                $renamed1 = uniqid(rand(), true).'.'."$ext";
                if(!move_uploaded_file($_FILES['product-image1']['tmp_name'], "img/".$renamed1)) {
                    $renamed1 = null;
                }
            }
        }
        else{
            $renamed1 = null;
        }

        if(isset($_FILES['product-image2'])) {
            if(in_array(strtolower($_FILES['product-image2']['type']), $allowed)) {
                $tmp2 = explode('.', $_FILES['product-image2']['name']);
                $ext2 = end($tmp2);
                $renamed2 = uniqid(rand(), true).'.'."$ext2";
                if(!move_uploaded_file($_FILES['product-image2']['tmp_name'], "img/".$renamed2)) {
                    $renamed2 = null;
                }
            }
        }
        else{
            $renamed2 = null;
        }
    	if(empty($errors)) {
    		$stmt = $conn->prepare("INSERT INTO Variants (ProductId, SKU, Color, Size, Quantity, Image1, Image2) VALUES (?, ?, ?, ?, ?, ?, ?)");
      		$stmt->bind_param("isssiss", $productId, $SKU, $Color, $Size, $Quantity, $renamed1, $renamed2);
           	$stmt->execute();
            $stmt->store_result();
		    if($stmt->affected_rows == 1) {
		    	echo "YES";
		    }
		    else{
		    	echo "NO";
		    }
		}
    	else{
    		echo "NO";
    	}
  	}	
  	else{
  		echo "NO";
  	}
?>