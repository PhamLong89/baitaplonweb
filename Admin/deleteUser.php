<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>

<?php
    adminAccess();
    if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
        $userId = $_GET['id'];
        $sql = "SELECT UserName, NumberPhone, Address, Email FROM Users WHERE UserID = ?";
        if($stmt = $conn->prepare($sql)) {
            $stmt->bind_param('i', $userId);
            $stmt->execute();           
            $result = $stmt->get_result();
            if($result->num_rows == 1){
                $user = $result->fetch_assoc();
            }
            else{
                redirect_to("Admin/viewUser.php");
            }       
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $sql = "DELETE FROM Users WHERE UserId = ?";
            if($stmt = $conn->prepare($sql)) {
                $stmt->bind_param('i', $userId);
                $stmt->execute();
                if($stmt->affected_rows == 1) {
                    $message = "<p class='success'>Xóa tài khoản khách hàng thành công</p>";
                } else {
                    $message = "<p class='error2'>Xóa tài khoản khách hàng thất bại</p>";
                }
                $stmt->close();
            }
            $conn->close();
        }
    }
    else{
        redirect_to("Admin/viewUser.php");
    }
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Xóa khách hàng</h4>
                    <hr />
                    <?php  
                      if(isset($message)){
                        echo $message;
                      }
                    ?>
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Họ tên: <?php if(isset($user)) echo $user['UserName']; ?>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Số điện thoại: <?php if(isset($user)) echo $user['NumberPhone']; ?>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Địa chỉ: <?php if(isset($user)) echo $user['Address']; ?>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="" class="font-weight-bold">Email: <?php if(isset($user)) echo $user['Email']; ?>
                            </label>
                        </div>

                        
                        <button type="submit" class="btn btn-info mt-4">Xóa khách hàng</button>
                        <button type="button" class="btn btn-dark mt-4"><a class="text-light" href="viewUser.php">Hủy</a></button>
                    </form>
                </div>
            </main>
            <!-- page-content" -->
        </div>
    </body>
</html>