<?php
    include ('mysqliConnect.php');
    include("../Customer/function.php");
?>
<?php
	adminAccess();
	$sql = "SELECT AdminId, AdminName, NumberPhone, Sex, Address, Email, Image FROM Admins";
	$result = $conn->query($sql);
?>
<?php include 'sidebarAdmin.php';?>
			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger font-weight-bold text-center">Danh sách admin</h4>
                    <hr />
                    <form action="searchAdmin.php" method="get" class="input-group col-lg-4 p-0 pl-md-3 float-right" enctype="multipart/form-data">
	                    <input type="text" class="form-control" name="search" placeholder="Tìm kiếm..." />
	                    <div class="input-group-append">
	                        <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
	                    </div>
	                </form>
                    <div class="row table">
	                	<table class="table mt-4" id="" name="tableAdmins">
						    <thead>
						        <tr>
						            <th scope="col">Mã tài khoản</th>
						            <th scope="col">Tên admin</th>
						            <th scope="col">Số điện thoại</th>
						            <th scope="col">Giới tính</th>
						            <th scope="col">Địa chỉ</th>
						            <th scope="col">Email</th>
						            <th scope="col">Hình ảnh</th>
						        </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    		if ($result->num_rows > 0) {
			  							while($row = $result->fetch_assoc()) {
			  								echo 
			  								"<tr>
			  									<td>{$row['AdminId']}</td>
			  									<td>{$row['AdminName']}</td>
			  									<td>{$row['NumberPhone']}</td>
			  									<td>{$row['Sex']}</td>
			  									<td>{$row['Address']}</td>
			  									<td>{$row['Email']}</td>
			  									<td><img class='img-admin' src=\"img/{$row['Image']}\" alt='' /></td>
			  									<td><a class='' href='editAdmin.php?id={$row['AdminId']}'>Sửa</a></td>
												<td><a class='' href='deleteAdmin.php?id={$row['AdminId']}'>Xóa</a></td>
			  								</tr>";
			  							}
			  						}
			  						$conn->close();
						    	?>
						    </tbody>
						</table>
					</div>
                </div>
                <button type="button" class="btn btn-dark mt-4 ml-5"><a class="text-light" href="admin.php">Thoát</a></button>
            </main>
            <!-- page-content" -->
        </div>
    </body>
</html>