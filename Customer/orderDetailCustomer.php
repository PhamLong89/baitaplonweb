<?php include '../Admin/mysqliConnect.php'; ?>
<?php include 'function.php'; ?>
<?php 
	if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
		$orderId = $_GET['id'];
		$userId = $_SESSION['UserID'];
		$sql = "SELECT OrderDetails.SKU, Products.ProductName, Variants.Color, Variants.Size, OrderDetails.Quantity, OrderDetails.Price, OrderDetails.Sale, tinhThanhTien(OrderDetails.OrderDetailId) FROM OrderDetails INNER JOIN Variants ON OrderDetails.SKU = Variants.SKU INNER JOIN Products ON Variants.ProductId = Products.ProductId INNER JOIN Orders ON Orders.OrderId = OrderDetails.OrderId WHERE Orders.OrderId = ? AND Orders.UserId = ?";
	    if($stmt = $conn->prepare($sql)) {
	        $stmt->bind_param('ii', $orderId, $userId);
	        $stmt->execute();           
	        $result = $stmt->get_result();
	    }
	}
?>
<?php include 'header.php'; ?>
	<div class="container-fluid">
		<h3 class="text-uppercase font-weight-bold mt-3 title-cart" style="font-size: 30px !important;">Chi tiết đơn hàng: <?php echo $orderId;?></h3>
		<hr class="clearfix w-100 " />
		<div class="row table2" style="overflow-x:auto;">
        	<table class="table mt-4" id="" name="tableOrderDetail">
			    <thead>
			        <tr>
			        	<th scope="col">SKU</th>
			            <th scope="col">Tên sản phẩm</th>
			            <th scope="col">Màu sắc</th>
			            <th scope="col">Kích cỡ</th>
			            <th scope="col">Giá bán</th>
			            <th scope="col">Số lượng</th>
			            <th scope="col">Giảm giá</th>
			            <th scope="col">Thành tiền</th>
			        </tr>
			    </thead>
			    <tbody>
			    	<?php 
			    		if ($result->num_rows > 0) {
  							while($row = $result->fetch_assoc()) {
  								echo 
  								"<tr>
  									<td>{$row['SKU']}</td>
  									<td>{$row['ProductName']}</td>
  									<td>{$row['Color']}</td>
  									<td>{$row['Size']}</td>
  									<td>{$row['Price']} đ</td>
  									<td>{$row['Quantity']}</td>
  									<td>{$row['Sale']} %</td>
  									<td>{$row['tinhThanhTien(OrderDetails.OrderDetailId)']} đ</td>
  								</tr>";
  							}
  						}
  						$conn->close();
			    	?>		
			    </tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php'; ?>	
