<?php include '../Admin/mysqliConnect.php'; ?>
<?php include 'function.php'; ?>
<?php 
	$userId = $_SESSION['UserID'];
	$sql = "SELECT OrderId, OrderDate, OrderStatus, tinhTongSanPham(OrderId), tinhTongTien(OrderId) FROM Orders WHERE UserId = ?";
    if($stmt = $conn->prepare($sql)) {
        $stmt->bind_param('i', $userId);
        $stmt->execute();           
        $result = $stmt->get_result();
    }
?>
<?php include 'header.php'; ?>
	<div class="container">
		<div class="row">
	        <div class="col-md-3 my-auto ">
	            <a class="row my-2" href="editUser.php">Thông tin tài khoản</a>
	            <a class="row my-2 link-active"  href="viewOrdersCustomer.php">Danh sách đơn hàng</a>
	            <a class="row my-2" href="changePasswordCustomer.php">Đổi mật khẩu</a>
	            <a class="row my-2" href="../Admin/logout.php">Đăng xuất</a>
	        </div>
	        <div class="col-md-9">
				<h3 class="text-uppercase font-weight-bold mt-3 title-cart" >Đơn hàng của bạn</h3>
				<hr class="clearfix w-100 " />
				<div class="row table2">
		        	<table class="table mt-4" id="" name="tableOrders">
					    <thead>
					        <tr>
					            <th scope="col">Mã đơn hàng</th>
					            <th scope="col">Ngày mua</th>
					            <th scope="col">Tổng sản phẩm</th>
					            <th scope="col">Tổng tiền</th>
					            <th scope="col">Trạng thái</th>
					        </tr>
					    </thead>
					    <tbody>
					    	<?php 
					    		if ($result->num_rows > 0) {
		  							while($row = $result->fetch_assoc()) {
		  								echo 
		  								"<tr>
		  									<td>{$row['OrderId']}</td>
		  									<td>{$row['OrderDate']}</td>
		  									<td>{$row['tinhTongSanPham(OrderId)']}</td>
		  									<td>{$row['tinhTongTien(OrderId)']} đ</td>
		  									<td>{$row['OrderStatus']}</td>
											<td><a class='' href='orderDetailCustomer.php?id={$row['OrderId']}'>Chi tiết</a></td>
		  								</tr>";
		  							}
		  						}
		  						$conn->close();
					    	?>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

<?php include 'footer.php'; ?>	