$(document).ready(function () {
    $(".slider-for").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: ".slider-nav",
        prevArrow: false,
        nextArrow: false,
    });
    $(".slider-nav").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: ".slider-for",
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        prevArrow: false,
        nextArrow: false,
        responsive: [
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                },
            },
        ],
    });
    $(".autoplay").slick({
        slidesToShow: 6,
        slidesToScroll: 6,
        autoplay: true,
        autoplaySpeed: 4000,
        prevArrow: false,
        nextArrow: false,
        responsive: [
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    autoplaySpeed: 4000,
                },
            },
        ],
    });

    $( "#color" ).change(function() {
        $("#size").find('option[value!= \"null\"]').remove();
        getSize($("#color").val(), $("#productId").val());
        getSKU($("#color").val(), $("#size").val(), $("#productId").val());
    });

    $( "#size" ).change(function() {
        getSKU($("#color").val(), $("#size").val(), $("#productId").val());
    });


    getSize($("#color").val(), $("#productId").val());    
});

function getSize(color, productId){
    $.ajax({
        type: "get",
        url: "getSize.php",
        data: "Color="+ color + "&productId=" + productId, 
        dataType: "json",
        success: function(response) {
            for (var i = 0; i < response.length; i++) {
                if(response[i] != ""){
                    var o = new Option(response[i], response[i]);
                    $(o).html(response[i]);
                    $("#size").append(o);
                }
                else{
                    var o = new Option(response[i], response[i]);
                    $(o).html("Free size");
                    $("#size").append(o);
                }           
            }
        },
    });
}

function getSKU(color, size, productId){
    $.ajax({
        type: "get",
        url: "getSKU.php",
        data: "Color="+ color + "&Size=" + size + "&productId=" + productId, 
        success: function(response) {
            $("#labelSKU").text("SKU: " + response);
            $("#SKU").val(response);
        },
    });
}

function quantityPlus(inputQuantity){
    var quantity = parseInt(inputQuantity.val());
    if(Number.isNaN(quantity)){
        quantity = 0;
    }
    inputQuantity.val(quantity + 1);
}

function quantityMinus(inputQuantity){
    var quantity = parseInt(inputQuantity.val());
    if(Number.isNaN(quantity)){
        quantity = 0;
    }
    if(quantity > 1){
        inputQuantity.val(quantity - 1);
    }
}

function isInputNumber(evt) {
    var ch = String.fromCharCode(evt.which);
    if (!/[0-9]/.test(ch)) {
        evt.preventDefault();
    }
}

