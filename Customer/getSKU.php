<?php include '../Admin/mysqliConnect.php'; ?>
<?php include 'function.php'; ?>
<?php 
	if(isset($_GET['Color']) && filter_var($_GET['Color'], FILTER_SANITIZE_STRING)  && isset($_GET['productId']) && filter_var($_GET['productId'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
		$Color = $conn->real_escape_string(trim($_GET['Color']));
        if(isset($_GET['Size']) && filter_var($_GET['Size'], FILTER_SANITIZE_STRING)){
            $Size = $conn->real_escape_string(trim($_GET['Size']));
        }
        else{
            $Size = "";
        }        
		$productId = $_GET['productId'];
		$sql = "SELECT SKU FROM Variants WHERE ProductId = ? AND Color = ? AND Size = ?";
		if($stmt = $conn->prepare($sql)) {
            $stmt->bind_param('iss', $productId, $Color, $Size);
            $stmt->execute();       
            $result = $stmt->get_result();
            if($result->num_rows > 0){
                $SKU = $result->fetch_assoc();
                echo $SKU['SKU'];
            }
            else{
                echo "";
            }       
        }
	}
?>