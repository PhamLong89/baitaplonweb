<?php
    include ('../Admin/mysqliConnect.php');
    include("function.php");
?>
<?php
    // if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
    if(isset($_SESSION['UserID'])){
        $userId = $_SESSION['UserID'];
        $sql = "SELECT UserName, NumberPhone, Address, Email FROM Users WHERE UserID = ?";
        if($stmt = $conn->prepare($sql)) {
            $stmt->bind_param('i', $userId);
            $stmt->execute();           
            $result = $stmt->get_result();
            if($result->num_rows == 1){
                $user = $result->fetch_assoc();
            }
            else{
                redirect_to("Customer/index.php");
            }       
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $errors = array();
            if(empty($_POST['nameUser'])){
            $errors[] = "nameUser";
            }else{
                $nameUser = $conn->real_escape_string(strip_tags($_POST['nameUser']));
                if(trim($nameUser) == ""){
                    $errors[] =  "nameUser";
                }
            }
        
            if(empty($_POST['phoneUser'])){
                $errors[] = "phoneUser";
            }
            else{
                $conn->real_escape_string(trim($_POST['phoneUser']));
                if(preg_match('/^[0]{1}[0-9]{9}$/', trim($_POST['phoneUser'])) || preg_match('/^[+]{1}[8]{1}[4]{1}[0-9]{9}$/', trim($_POST['phoneUser']))) {
                    $phoneUser = $conn->real_escape_string(trim($_POST['phoneUser']));
                } else {
                    $errors[] = "phoneUser2";
                }
            }

            if(empty($_POST['addressUser'])){
                $errors[] = "addressUser";
            }else{
                $addressUser = $conn->real_escape_string(strip_tags($_POST['addressUser']));
                if(trim($addressUser) == ""){
                    $errors[] = "addressUser";
                }
            }

            if(empty($errors)) {
                $sql = "UPDATE Users SET UserName = ?, NumberPhone = ?, Address = ? WHERE UserID = ?";
                if($upStmt = $conn->prepare($sql)) {
                    $upStmt->bind_param('sssi',$nameUser, $phoneUser, $addressUser, $userId);
                    $upStmt->execute();
                    if($upStmt->affected_rows == 1) {
                        $message = "<p class='success'>Sửa thông tin tài khoản thành công</p>";
                    }
                    else{
                        $message = "<p class='error2'>Sửa thông tin tài khoản thất bại</p>";
                    }           
                }
                $upStmt->close();
                $conn->close(); 
            }        
        }
    }
    else{
        redirect_to("Customer/index.php");
    }
?>
<?php include 'header.php'; ?>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-3 my-auto ">
            <a class="row my-2 link-active" href="editUser.php">Thông tin tài khoản</a>
            <a class="row my-2" href="viewOrdersCustomer.php">Danh sách đơn hàng</a>
            <a class="row my-2" href="changePasswordCustomer.php">Đổi mật khẩu</a>
            <a class="row my-2" href="../Admin/logout.php">Đăng xuất</a>
        </div>
        <div class="col-md-9">
            <h4 class="text-uppercase font-weight-bold title-cart">Sửa thông tin</h4>
            <hr />
            <?php if (isset($message)) {
                echo $message;
            } ?>
            <form action="" method="POST">
                <div class="form-group">
                        <label for="nameUser" class="font-weight-bold">Họ tên<span class="text-danger">*</span>
                        <?php
                            if(isset($errors) && in_array('nameUser', $errors)){
                                echo "<p class='error'>Vui lòng nhập họ và tên</p>";
                            }
                        ?>
                        </label>
                        <input type="text" class="form-control" id="nameUser" name="nameUser"  placeholder="Họ tên" value="<?php if(isset($_POST['nameUser'])) echo strip_tags($_POST['nameUser']); elseif(isset($user)) echo $user['UserName'];?>"/>
                    </div>
                    <div class="form-group">
                        <label for="phoneUser" class="font-weight-bold">Số điện thoại<span class="text-danger">*</span>
                        <?php
                            if(isset($errors) && in_array('phoneUser', $errors)){
                                echo "<p class='error'>Vui lòng nhập số điện thoại</p>";
                            }
                            elseif(isset($errors) && in_array('phoneUser2', $errors)){
                                echo "<p class='error'>Số điện thoại không đúng định dạng</p>";
                            }
                        ?>
                        </label>
                        <input type="tel" class="form-control" id="phoneUser" name="phoneUser"  placeholder="0987654321" value="<?php if(isset($_POST['phoneUser'])) echo strip_tags($_POST['phoneUser']); elseif(isset($user)) echo $user['NumberPhone'];?>"/>
                    </div>
                    <div class="form-group">
                        <label for="addressUser" class="font-weight-bold">Địa chỉ<span class="text-danger">*</span>
                        <?php
                            if(isset($errors) && in_array('addressUser', $errors)){
                                echo "<p class='error'>Vui lòng nhập địa chỉ</p>";
                            }
                        ?>
                        </label>
                        <input type="text" class="form-control" id="addressUser" name="addressUser"  placeholder="Địa chỉ" value="<?php if(isset($_POST['addressUser'])) echo strip_tags($_POST['addressUser']); elseif(isset($user)) echo $user['Address'];?>"/>
                    </div>

                <button type="submit" class="btn btn-info mt-4">Sửa thông tin</button>
            </form>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>  