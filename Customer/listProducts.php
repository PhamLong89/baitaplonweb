<?php include '../Admin/mysqliConnect.php'; ?>
<?php include 'function.php'; ?>
<?php 
    if(isset($_GET['category']) && filter_var($_GET['category'], FILTER_SANITIZE_STRING)){
        $category = $conn->real_escape_string(trim($_GET['category']));
        $category2 = "%" . $category . "%";
        if(isset($_GET['page']) && filter_var($_GET['page'], FILTER_VALIDATE_INT, array('min_range' => 1))) {
            $curentPage = $_GET['page'];
        } 
        else {
            $curentPage = 1;
        }
        $display = 4;
        //Lấy tổng số lượng sản phẩm
        $sql = "SELECT count(ProductId) FROM Products WHERE CategoryName LIKE ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('s', $category2);
        $stmt->execute();           
        $result = $stmt->get_result();
        $countProduct = $result->fetch_assoc();
        if($countProduct['count(ProductId)'] > $display){
            $page = ceil($countProduct['count(ProductId)']/$display);
            if($curentPage > $page){
                $curentPage = $page;
            }
        }
        else{
            $page = 1;
        }
        $listProducts = array();
        //Lấy sản phẩm tương ứng của trang
        $sql = "SELECT ProductId, ProductName, ProductPrice, Sale FROM Products WHERE CategoryName LIKE ? LIMIT " . (($curentPage - 1)*$display) . ", ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('si', $category2, $display);
        $stmt->execute();           
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            //Lấy ảnh của sản phẩm
            $sql = "SELECT Variants.ProductId, ProductName, ProductPrice, Sale, Image1, Image2 FROM Variants INNER JOIN Products ON Variants.ProductId = Products.ProductId WHERE Variants.ProductId = ? AND (Image1 IS NOT NULL OR Image2 IS NOT NULL) LIMIT 1";
            if($stmt = $conn->prepare($sql)) {
                $stmt->bind_param('i', $row['ProductId']);
                $stmt->execute();           
                $result2 = $stmt->get_result();
                if($result2->num_rows == 1){
                    $listProducts[] = $result2->fetch_assoc();
                }
                else{
                    $listProducts[] = $row;
                }
            }
        }
    }
?>
<?php include 'header.php'; ?>
<div class="container-fluid">
	<div class="row">
        <!-- Hiển thị sidebar -->
		<div class="col-md-2 col-12 d-none d-md-block">
			<div class="mt-5 ml-3">
                <?php 
                    switch ($category) {
                        case 'Nữ':
                            echo "<a class='font-weight-bold link-active' href='listProducts.php?category=Nữ'>Nữ</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo nữ'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần nữ'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Váy nữ'>Váy</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện nữ'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Áo nữ':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Nữ'>Nữ</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a class='link-active' href='listProducts.php?category=Áo nữ'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần nữ'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Váy nữ'>Váy</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện nữ'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Quần nữ':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Nữ'>Nữ</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo nữ'>Áo</a></li>
                                    <li><a class='link-active' href='listProducts.php?category=Quần nữ'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Váy nữ'>Váy</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện nữ'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Váy nữ':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Nữ'>Nữ</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo nữ'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần nữ'>Quần</a></li>
                                    <li><a class='link-active' href='listProducts.php?category=Váy nữ'>Váy</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện nữ'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Phụ kiện nữ':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Nữ'>Nữ</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo nữ'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần nữ'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Váy nữ'>Váy</a></li>
                                    <li><a class='link-active' href='listProducts.php?category=Phụ kiện nữ'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Nam':
                            echo "<a class='font-weight-bold link-active' href='listProducts.php?category=Nam'>Nam</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo nam'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần nam'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện nam'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Áo nam':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Nam'>Nam</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a class='link-active' href='listProducts.php?category=Áo nam'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần nam'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện nam'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Quần nam':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Nam'>Nam</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo nam'>Áo</a></li>
                                    <li><a class='link-active' href='listProducts.php?category=Quần nam'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện nam'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Phụ kiện nam':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Nam'>Nam</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo nam'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần nam'>Quần</a></li>
                                    <li><a class='link-active' href='listProducts.php?category=Phụ kiện nam'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Bé gái':
                            echo "<a class='font-weight-bold link-active' href='listProducts.php?category=Bé gái'>Bé gái</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo bé gái'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần bé gái'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Váy bé gái'>Váy</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện bé gái'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Áo bé gái':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Bé gái'>Bé gái</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a class='link-active' href='listProducts.php?category=Áo bé gái'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần bé gái'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Váy bé gái'>Váy</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện bé gái'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Quần bé gái':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Bé gái'>Bé gái</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo bé gái'>Áo</a></li>
                                    <li><a class='link-active' href='listProducts.php?category=Quần bé gái'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Váy bé gái'>Váy</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện bé gái'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Váy bé gái':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Bé gái'>Bé gái</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo bé gái'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần bé gái'>Quần</a></li>
                                    <li><a class='link-active' href='listProducts.php?category=Váy bé gái'>Váy</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện bé gái'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Phụ kiện bé gái':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Bé gái'>Bé gái</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo bé gái'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần bé gái'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Váy bé gái'>Váy</a></li>
                                    <li><a class='link-active' href='listProducts.php?category=Phụ kiện bé gái'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Bé trai':
                            echo "<a class='font-weight-bold link-active' href='listProducts.php?category=Bé trai'>Bé trai</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo bé trai'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần bé trai'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện bé trai'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Áo bé trai':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Bé trai'>Bé trai</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a class='link-active' href='listProducts.php?category=Áo bé trai'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần bé trai'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện bé trai'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Quần bé trai':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Bé trai'>Bé trai</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo bé trai'>Áo</a></li>
                                    <li><a class='link-active' href='listProducts.php?category=Quần bé trai'>Quần</a></li>
                                    <li><a href='listProducts.php?category=Phụ kiện bé trai'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        case 'Phụ kiện bé trai':
                            echo "<a class='font-weight-bold' href='listProducts.php?category=Bé trai'>Bé trai</a>";
                            echo "<p class='font-weight-bold mb-1 mt-2'>Danh mục sản phẩm</p>";
                            echo "
                                <ul class='list-unstyled'>
                                    <li><a href='listProducts.php?category=Áo bé trai'>Áo</a></li>
                                    <li><a href='listProducts.php?category=Quần bé trai'>Quần</a></li>
                                    <li><a class='link-active' href='listProducts.php?category=Phụ kiện bé trai'>Phụ kiện</a></li>
                                </ul>";
                            break;
                        
                        default:
                            break;
                    }
                ?>
			</div>
		</div>
        <!-- Hiển thị danh sách sản phẩm -->
		<div class="col-md-10 col-12">
			<div class="row">
                <?php
                    foreach ($listProducts as $key => $pro) {
                        if($pro['Sale'] == 0){
                            echo"
                                <div class='col-md-3 col-6 p-1 p-md-3'>
                                    <div class='card'>
                                        <a href=\"productDetail.php?id={$pro['ProductId']}\"><img class='card-img-top p-md-3' src=\"../Admin/img/";
                            if($pro['Image1'] != null){
                                echo $pro['Image1'];
                            }
                            else{
                                echo $pro['Image2'];
                            }
                            echo "\" alt=\"{$pro['ProductName']}\" /></a>
                                        <div class='card-body'>
                                            <p class='card-title text-center text-truncate'><a href=\"productDetail.php?id={$pro['ProductId']}\" class='text-decoration-none'>{$pro['ProductName']}</a></p>
                                            <h5 class='card-text font-weight-bold text-center'>{$pro['ProductPrice']} đ</h5>
                                        </div>
                                    </div>
                                </div>
                                ";
                        }
                        else{
                            echo"
                                <div class='col-md-3 col-6 p-1 p-md-3'>
                                    <div class='card'>
                                        <div class='ribbon-wrapper'>
                                            <div class='ribbon'>Giảm {$pro['Sale']}%</div>
                                        </div>
                                        <a href=\"productDetail.php?id={$pro['ProductId']}\"><img class='card-img-top p-md-3' src=\"../Admin/img/";
                            if($pro['Image1'] != null){
                                echo $pro['Image1'];
                            }
                            else{
                                echo $pro['Image2'];
                            }
                            echo "\" alt=\"{$pro['ProductName']}\" /></a>
                                        <div class='card-body'>
                                            <p class='card-title text-center text-truncate'><a href=\"productDetail.php?id={$pro['ProductId']}\" class='text-decoration-none'>{$pro['ProductName']}</a></p>
                                            <h5 class='card-text font-weight-bold text-center'>{$pro['ProductPrice']} đ</h5>
                                        </div>
                                    </div>
                                </div>
                                ";
                        }
                    }
                ?>
			</div>
		</div>
	</div>
    <!-- Hiển thị phân trang -->
	<div class="row justify-content-center">
		<nav aria-label="Page navigation example" class="">
		    <ul class="pagination">
		        <li class="page-item">
		            <a class="page-link" href="<?php if($curentPage > 1) echo "listProducts.php?category={$category}&page=" . ($curentPage - 1);?>" aria-label="Previous">
		                <span aria-hidden="true">&laquo;</span>
		                <span class="sr-only">Previous</span>
		            </a>
		        </li>
                <?php
                    for($i = 1; $i <= $page; $i++ ){
                        if($curentPage == $i){
                            echo "<li class='page-item'><a class='page-link text-danger' href='listProducts.php?category={$category}&page={$i}'>{$i}</a></li>";
                        }
                        else{
                            echo "<li class='page-item'><a class='page-link' href='listProducts.php?category={$category}&page={$i}'>{$i}</a></li>";
                        }
                    }
                ?>
		        <li class="page-item">
                    <a class="page-link" href="<?php if($curentPage < $page) echo "listProducts.php?category={$category}&page=" . ($curentPage + 1);?>" aria-label="Next">
		                <span aria-hidden="true">&raquo;</span>
		                <span class="sr-only">Next</span>
		            </a>
		        </li>
		    </ul>
		</nav>
	</div>

</div>
<?php include 'footer.php'; ?>			