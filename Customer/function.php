<?php
    ini_set('session.use_only_cookies', true);
    session_start();
?> 
<?php
	define('BASE_URL', 'http://localhost/BaiTapLonWeb/');
    define('LIVE', FALSE);

    function redirect_to($page = 'index.php') {
        $url = BASE_URL . $page;
        header("Location: $url");
        exit();
    }

    function adminAccess(){
    	if(!isset($_SESSION['AdminId'])){
    		redirect_to("Customer/index.php");
    	}
    }
    
    function the_content($text) {
        $sanitized = htmlentities($text, ENT_COMPAT, 'UTF-8');
        return str_replace(array("\r\n", "\n"),array("<p>", "</p>"),$sanitized);
    }
?>