<?php include '../Admin/mysqliConnect.php'; ?>
<?php include 'function.php'; ?>
<?php 
	if(isset($_GET['Color']) && filter_var($_GET['Color'], FILTER_SANITIZE_STRING) && isset($_GET['productId']) && filter_var($_GET['productId'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
		$listSize = array();
		$Color = $conn->real_escape_string(trim($_GET['Color']));
		$productId = $_GET['productId'];
		$sql = "SELECT DISTINCT Size FROM Variants WHERE ProductId = ? AND Color = ?";
		if($stmt = $conn->prepare($sql)) {
            $stmt->bind_param('is', $productId, $Color);
            $stmt->execute();       
            $result = $stmt->get_result();
            while($size = $result->fetch_assoc()) {
                $listSize[] = $size['Size'];   
            }  
        }
        echo json_encode($listSize);
	}
?>