<?php include '../Admin/mysqliConnect.php'; ?>
<?php include 'function.php'; ?>
<?php 
    $listProducts = array();
    $sql = "SELECT ProductId FROM Products ORDER BY SaleDate DESC LIMIT 6";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $sql = "SELECT Variants.ProductId, ProductName, ProductPrice, Sale, Image1, Image2 FROM Variants INNER JOIN Products ON Variants.ProductId = Products.ProductId WHERE Variants.ProductId = ? AND (Image1 IS NOT NULL OR Image2 IS NOT NULL) LIMIT 1";
        if($stmt = $conn->prepare($sql)) {
            $stmt->bind_param('i', $row['ProductId']);
            $stmt->execute();           
            $result2 = $stmt->get_result();
            if($result2->num_rows == 1){
                $listProducts[] = $result2->fetch_assoc();
            }
        }
    }
?>
<?php include 'header.php'; ?>
<!-- slide -->
        <div id="carouselExampleIndicators" class="carousel slide mt-0" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="img/slide1.jpg" alt="" />
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/slide2.jpg" alt="" />
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/slide3.jpg" alt="" />
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/slide4.jpg" alt="" />
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- end slide -->

        <!-- banner -->
        <div class="container-fluid mt-2">
            <div class="row">
                <div class="col-md-6 pl-0 pr-0">
                    <a class="w-100 h-auto" href=""><img class="w-100 h-auto" src="img/banner1.jpg" alt="" /></a>
                </div>
                <div class="col-md-6 pl-0 pr-0">
                    <a class="w-100 h-auto" href=""><img class="w-100 h-auto" src="img/banner2.jpg" alt="" /></a>
                </div>
            </div>
        </div>

        <!-- list product -->
        <div class="container-fluid p-0">
            <div class="row m-0 bg-dark pt-3 pb-2">
                <div class="col-md-4">
                    <h3 class="text-white font-weight-bold" style="font-size: 25px !important;">SẢN PHẨM MỚI</h3>
                </div>
                <div class="col-md-8 p-0">
                    <div class="row m-0">
                        <div class="col-3">
                            <a class="w-100 h-auto text-decoration-none text-white" href="listProducts.php?category=Nữ">NỮ</a>
                        </div>
                        <div class="col-3">
                            <a class="w-100 h-auto text-decoration-none text-white" href="listProducts.php?category=Nam">NAM</a>
                        </div>
                        <div class="col-3">
                            <a class="w-100 h-auto text-decoration-none text-white" href="listProducts.php?category=Bé gái">BÉ GÁI</a>
                        </div>
                        <div class="col-3">
                            <a class="w-100 h-auto text-decoration-none text-white" href="listProducts.php?category=Bé trai">BÉ TRAI</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Hiển thị sản phẩm -->
            <div class="container-fluid mt-3 p-0">
                <div class="row w-100 m-0">
                    <?php
                        foreach ($listProducts as $key => $pro) {
                            if($pro['Sale'] == 0){
                                echo"
                                    <div class='col-md-4 col-6 p-1 p-md-3'>
                                        <div class='card'>
                                            <a href=\"productDetail.php?id={$pro['ProductId']}\"><img class='card-img-top p-md-3' src=\"../Admin/img/";
                                if($pro['Image1'] != null){
                                    echo $pro['Image1'];
                                }
                                else{
                                    echo $pro['Image2'];
                                }
                                echo "\" alt=\"{$pro['ProductName']}\" /></a>
                                            <div class='card-body'>
                                                <p class='card-title text-center text-truncate'><a href=\"productDetail.php?id={$pro['ProductId']}\" class='text-decoration-none'>{$pro['ProductName']}</a></p>
                                                <h5 class='card-text font-weight-bold text-center'>{$pro['ProductPrice']} đ</h5>
                                                
                                            </div>
                                        </div>
                                    </div>
                                ";
                            }
                            else{
                                echo"
                                    <div class='col-md-4 col-6 p-1 p-md-3'>
                                        <div class='card'>
                                            <div class='ribbon-wrapper'>
                                                <div class='ribbon'>Giảm {$pro['Sale']}%</div>
                                            </div>
                                            <a href=\"productDetail.php?id={$pro['ProductId']}\"><img class='card-img-top p-md-3' src=\"../Admin/img/";
                                if($pro['Image1'] != null){
                                    echo $pro['Image1'];
                                }
                                else{
                                    echo $pro['Image2'];
                                }
                                echo "\" alt=\"{$pro['ProductName']}\" /></a>
                                            <div class='card-body'>
                                                <p class='card-title text-center text-truncate'><a href=\"productDetail.php?id={$pro['ProductId']}\" class='text-decoration-none '>{$pro['ProductName']}</a></p>
                                                <h5 class='card-text font-weight-bold text-center '>{$pro['ProductPrice']} đ</h5>
                                            </div>
                                        </div>
                                    </div>
                                    ";
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <!-- end list product -->

        <!-- banner bottom -->
        <div class="container-fluid m-0 p-0">
            <a class="w-100 h-auto" href="#"><img class="w-100 h-auto" src="img/banner3.jpg" alt="" /></a>
        </div>

<?php include 'footer.php'; ?>