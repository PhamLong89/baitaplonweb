<?php
    include ('../Admin/mysqliConnect.php');
    include("function.php");
?>
<?php 
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        $errors = array();
        if(empty($_POST['emailUser'])){
            $errors[] = 'emailUser';
        }
        else{
          if(preg_match('/^\w+@[a-zA-Z]{3,}\.com$/i', trim($_POST['emailUser']))) {
                $emailUser = $conn->real_escape_string(strip_tags($_POST['emailUser']));
            } else {
                $errors[] = "emailUser";
            }
        }      
        if(preg_match('/^[\w\'.-]{4,20}$/', trim($_POST['password']))) {
            $password = $conn->real_escape_string(trim($_POST['password']));
        } else {
            $errors[] = 'password';
        }
        if(empty($errors)) {

	        $sql = "SELECT UserID, UserName, Address FROM Users WHERE Email = ? AND PassWord = ?";
	        if($stmt = $conn->prepare($sql)) {
	            $stmt->bind_param('ss', $emailUser, sha1($password));
	            $stmt->execute();           
	            $result = $stmt->get_result();
	            if($result->num_rows == 1){
	            	session_regenerate_id();
	                $user = $result->fetch_assoc();
	                $_SESSION['UserID'] = $user['UserID'];
	                $_SESSION['UserName'] = $user['UserName'];
	                $_SESSION['Email'] = $emailUser;
	                $_SESSION['Address'] = $user['Address'];
	                redirect_to("Customer/index.php");
	            }
	            else{
	            	$message = "<p class='error2'>Tên tài khoản hoặc mật khẩu không chính xác</p>";
	            }       
	        }
        } else {
            $message = "<p class='error2'>Tên tài khoản hoặc mật khẩu không chính xác</p>";
        }
    
    } // END MAIN IF
?>
<?php include 'header.php'; ?>

<div class="container-fluid mt-5">
	<div class="row">
		<div class="col-md-6 text-uppercase text-center my-auto register" >
			Đăng nhập
		</div>
		<div class="col-md-6 bg-info p-md-4">
			<?php  
                if(isset($message)){
                    	echo $message;
                    }
                ?>
			<form class="m-md-4" action="" method="post">
				<div class="form-group">
				    <label for="emailUser" class="font-weight-bold">Email <span class="text-danger">*</span></label>
				    <input type="email" class="form-control" id="emailUser" name="emailUser" aria-describedby="emailHelp" placeholder="Email" />
				</div>
				<div class="form-group">
				    <label for="password" class="font-weight-bold">Mật khẩu <span class="text-danger">*</span></label>
				    <input type="password" class="form-control" id="password" name="password" placeholder="Mật khẩu" />
				</div>
				<button type="submit" class="btn btn-dark text-white my-3 mt-md-5">Đăng nhập</button>
			</form>
			<p class="d-inline mr-2 ml-md-4">Bạn chưa có tài khoản?</p>
			<a class="font-weight-bold" href="register.php">Đăng ký</a>
			<a class="font-weight-bold d-block ml-md-4 my-3" href="">Quên mật khẩu?</a>
		</div>
	</div>
</div>

<?php include 'footer.php'; ?>	