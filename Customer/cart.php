<?php include '../Admin/mysqliConnect.php'; ?>
<?php include 'function.php'; ?>
<?php 
	if(isset($_SESSION['cart'])){
		$cart = $_SESSION['cart'];
		$address = $_SESSION['Address'];
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if(!isset($_SESSION['UserID'])){
				redirect_to("Customer/login.php");
			}
			else{
				$userId = $_SESSION['UserID'];
				if(isset($cart) && count($cart) > 0){
					if(empty($_POST['address'])){
			            $errors[] = "address";
			        }else{
			            $address = $conn->real_escape_string(strip_tags($_POST['address']));
			            if(trim($address) == ""){
			                $errors[] = "address";
			            }
			        }
			        if(empty($errors)){
			        	$status = "Chưa giao";
						$pay = "Chưa thanh toán";
						$stmt = $conn->prepare("INSERT INTO Orders (OrderDate, UserId, OrderStatus, OrderPay, OrderAdress) VALUES (?, ?, ?, ?, ?)");
					    $stmt->bind_param("sisss", date("Y-m-d"), $userId, $status, $pay, $address);
				      	$stmt->execute();
				      	$stmt->store_result();
				      	if($stmt->affected_rows == 1) {
				      		$sql = "SELECT MAX(OrderId) FROM Orders";
					        $result = $conn->query($sql);
					        if ($result->num_rows > 0) {
					        	$order = $result->fetch_assoc();
					          	$orderId = $order['MAX(OrderId)'];
					          	foreach ($cart as $key => $item) {
					          		$stmt = $conn->prepare("INSERT INTO OrderDetails (OrderId, SKU, Quantity, Price, Sale) VALUES (?, ?, ? ,?, ?)");
								    $stmt->bind_param("isiii", $orderId, $item['SKU'], $item['Quantity'], $item['Price'], $item['Sale']);
							      	$stmt->execute();
							      	$stmt->store_result();
					          	}
					          	unset($_SESSION['cart']);
					          	echo"<script>
				                        alert('Đặt hàng thành công');
				                    </script>";
					        }
				      	}
				      	else{
				      		echo"<script>
			                        alert('Đặt hàng thất bại. Vui lòng thử lại sau');
			                    </script>";
				      	}
			        }
				}
			}
		}
	}
?>
<?php include 'header.php'; ?>
<div class="container-fluid">
	<h3 class="text-uppercase font-weight-bold mt-3 title-cart" style="font-size: 30px !important;">Giỏ Hàng</h3>
	<hr class="clearfix w-100 " />
	<form action="" method="POST">
		<div class="form-group container <?php if(!isset($cart) || count($cart) == 0) echo "d-none"; ?>">
            <label for="address" class="font-weight-bold">Địa chỉ giao hàng<span class="text-danger">*</span>
            <?php
              if(isset($errors) && in_array('address', $errors)){
                echo "<p class='error'>Vui lòng nhập địa chỉ giao hàng</p>";
              }
            ?>
            </label>
            <input type="text" class="form-control" id="address" name="address"  placeholder="Địa chỉ" value="<?php if(isset($_POST['address'])) echo strip_tags($_POST['address']); else echo $address; ?>"/>
        </div>
        <hr class="clearfix w-100 " />
		<div class="row">
			<?php 
				if(isset($cart) && count($cart) > 0){
					
					//Hiển thị danh sách items cart
					$totalProduct = 0;
					$totalPrice = 0;
					$totalSale = 0;
					$pay = 0;
					echo "<div class='col-md-8 col-12'>";
					foreach ($cart as $key => $item) {
						$price = $item['Price'] * $item['Quantity'];
						$sale = round($item['Price'] * $item['Sale']/100) * $item['Quantity'];
						$total = $price - $sale;
						echo "
						<div class='row'>
							<div class='col-3'>
								<img src='../Admin/img/{$item['Image']}' alt='' class='img-thumbnail'>
							</div>
							<div class='col-9 pl-md-5'>
								<p class='font-weight-bold text-uppercase'>{$item['ProductName']}</p>
								<p class='my-1'>SKU: {$item['SKU']}</p>
								<p class='my-1'>Màu sắc: {$item['Color']}</p>
								<p class='my-1'>Kích cỡ: {$item['Size']}</p>
								<p class='my-1'>Đơn giá: {$price}đ</p>
								<p class='my-1'>Giảm giá: {$sale}đ</p>
								<p class='d-inline'>Số lượng: </p>
								<div class='btn-group btn-group-sm' role='group' aria-label='Basic example'>
						            <button type='button' class='btn btn-secondary quantity-minus' onclick='quantityMinus($(this).next())'><i class='fas fa-minus'></i></button>
						            <input class='text-center quantity' type='number' name='' value='{$item['Quantity']}' min='1' onkeypress='isInputNumber(event)' style='width:50px;'>
						            <button type='button' class='btn btn-secondary quantity-plus' onclick='quantityPlus($(this).prev())'><i class='fas fa-plus'></i></button>
						        </div>
								<p class='my-2'>Thành tiền: {$total}</p>
								<a class='float-right mr-3 mr-md-5' href='deleteCartItem.php?SKU={$item['SKU']}'>Xóa</a>
							</div>
						</div>
						<hr class='clearfix w-100' />";	

						$totalProduct += $item['Quantity'];
						$totalPrice += $price;
						$totalSale += $sale;
						$pay += $total;			
					}
					echo "</div>";
					//Hiển thị tổng 
					echo "
					<div class='col-md-4 col-12'>
						<div class='container bg-light'>
							<div class='row pt-5'>
								<div class='col-6'>
									<p class='text-left'>Tổng:</p>
								</div>
								<div class='col-5'>
									<p class='text-right'>{$totalProduct} sản phẩm</p>
								</div>				
							</div>
							<div class='row'>
								<div class='col-6'>
									<p class='text-left'>Tổng giá bán:</p>
								</div>
								<div class='col-5'>
									<p class='text-right'>{$totalPrice} đ</p>
								</div>				
							</div>
							<div class='row'>
								<div class='col-6'>
									<p class='text-left'>Giảm giá:</p>
								</div>
								<div class='col-5'>
									<p class='text-right'>{$totalSale} đ</p>
								</div>				
							</div>
							<div class='row'>
								<div class='col-6'>
									<p class='text-left font-weight-bold'>Tạm tính:</p>
								</div>
								<div class='col-5'>
									<p class='text-right font-weight-bold text-danger'>{$pay} đ</p>
								</div>				
							</div>
							<div class='row'>
								<button type='button' class='btn btn-secondary text-uppercase font-weight-bold mb-5 float-left mr-4'>Tiếp tục mua hàng</button>
								<button type='submit' class='btn btn-danger text-uppercase font-weight-bold mb-5 float-right'>Đặt hàng</button>	
							</div>							
						</div>
					</div>";
				}
				else{
					echo "<p class='font-weight-bold ml-5'>Giỏ hàng trống. Vui lòng tiếp tục mua hàng</p>";
				}
			?>
		</div>		
	</form>
</div>


<?php include 'footer.php'; ?>		