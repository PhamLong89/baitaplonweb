<?php include '../Admin/mysqliConnect.php'; ?>
<?php include 'function.php'; ?>
<?php 
	if($_SERVER['REQUEST_METHOD'] == 'GET'){
		if(isset($_SESSION['cart'])){
			session_regenerate_id();
			$cart = $_SESSION['cart'];
			if(isset($_GET['SKU']) && filter_var($_GET['SKU'], FILTER_SANITIZE_STRING)) {
				echo "long";
				$SKU = $conn->real_escape_string(trim($_GET['SKU']));
				echo $SKU;
				foreach ($cart as $key => $item) {
					if($item['SKU'] == $SKU){
						unset($cart[$key]);
						break;
					}
				}
				$_SESSION['cart'] = $cart;
				redirect_to("Customer/cart.php");
			}
			else{
				redirect_to("Customer/cart.php");
			}
		}
		else{
			redirect_to("Customer/cart.php");
		}
	}
?>