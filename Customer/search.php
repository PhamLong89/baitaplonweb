<?php include '../Admin/mysqliConnect.php'; ?>
<?php include 'function.php'; ?>
<?php
	if(isset($_GET['search']) && filter_var($_GET['search'], FILTER_SANITIZE_STRING)){
		$search = $conn->real_escape_string(trim($_GET['search']));
		$search2 = "%" . $search . "%";
		if(isset($_GET['page']) && filter_var($_GET['page'], FILTER_VALIDATE_INT, array('min_range' => 1))) {
            $curentPage = $_GET['page'];
        } 
        else {
            $curentPage = 1;
        }
        $display = 4;
        //Lấy tổng số lượng sản phẩm
        $sql = "SELECT count(ProductId) FROM Products WHERE ProductName LIKE ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('s', $search2);
        $stmt->execute();           
        $result = $stmt->get_result();
        $countProduct = $result->fetch_assoc();
        if($countProduct['count(ProductId)'] > $display){
            $page = ceil($countProduct['count(ProductId)']/$display);
            if($curentPage > $page){
                $curentPage = $page;
            }
        }
        else{
            $page = 1;
        }
        $listProducts = array();
        //Lấy sản phẩm tương ứng của trang
        $sql = "SELECT ProductId, ProductName, ProductPrice, Sale FROM Products WHERE ProductName LIKE ? LIMIT " . (($curentPage - 1)*$display) . ", ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('si', $search2, $display);
        $stmt->execute();           
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            //Lấy ảnh của sản phẩm
            $sql = "SELECT Variants.ProductId, ProductName, ProductPrice, Sale, Image1, Image2 FROM Variants INNER JOIN Products ON Variants.ProductId = Products.ProductId WHERE Variants.ProductId = ? AND (Image1 IS NOT NULL OR Image2 IS NOT NULL) LIMIT 1";
            if($stmt = $conn->prepare($sql)) {
                $stmt->bind_param('i', $row['ProductId']);
                $stmt->execute();           
                $result2 = $stmt->get_result();
                if($result2->num_rows == 1){
                    $listProducts[] = $result2->fetch_assoc();
                }
                else{
                    $listProducts[] = $row;
                }
            }
        }
	}
?>
<?php include 'header.php'; ?>
<div class="container-fluid">
	<p class="text-uppercase font-weight-bold mt-5">Kết quả tìm kiếm cho: <?php echo $search;?></p>
	<div class="row">
        <!-- Hiển thị sản phẩm tim được -->
		<?php
            foreach ($listProducts as $key => $pro) {
                if($pro['Sale'] == 0){
                    echo"
                        <div class='col-md-3 col-6 p-1 p-md-3'>
                            <div class='card'>
                                <a href=\"productDetail.php?id={$pro['ProductId']}\"><img class='card-img-top p-md-3' src=\"../Admin/img/";
                    if($pro['Image1'] != null){
                        echo $pro['Image1'];
                    }
                    else{
                        echo $pro['Image2'];
                    }
                    echo "\" alt=\"{$pro['ProductName']}\" /></a>
                                <div class='card-body'>
                                    <p class='card-title text-center text-truncate'><a href=\"productDetail.php?id={$pro['ProductId']}\" class='text-decoration-none'>{$pro['ProductName']}</a></p>
                                    <h5 class='card-text font-weight-bold text-center'>{$pro['ProductPrice']} đ</h5>
                                </div>
                            </div>
                        </div>
                        ";
                }
                else{
                    echo"
                        <div class='col-md-3 col-6 p-1 p-md-3'>
                            <div class='card'>
                                <div class='ribbon-wrapper'>
                                    <div class='ribbon'>Giảm {$pro['Sale']}%</div>
                                </div>
                                <a href=\"productDetail.php?id={$pro['ProductId']}\"><img class='card-img-top p-md-3' src=\"../Admin/img/";
                    if($pro['Image1'] != null){
                        echo $pro['Image1'];
                    }
                    else{
                        echo $pro['Image2'];
                    }
                    echo "\" alt=\"{$pro['ProductName']}\" /></a>
                                <div class='card-body'>
                                    <p class='card-title text-center text-truncate'><a href=\"productDetail.php?id={$pro['ProductId']}\" class='text-decoration-none'>{$pro['ProductName']}</a></p>
                                    <h5 class='card-text font-weight-bold text-center'>{$pro['ProductPrice']} đ</h5>
                                </div>
                            </div>
                        </div>
                        ";
                }
            }
        ?>
	</div>
    <!-- Hiển thị phân trang -->
	<div class="row justify-content-center">
		<nav aria-label="Page navigation example" class="">
		    <ul class="pagination">
		        <li class="page-item">
		            <a class="page-link" href="<?php if($curentPage > 1) echo "search.php?search={$search}&page=" . ($curentPage - 1);?>" aria-label="Previous">
		                <span aria-hidden="true">&laquo;</span>
		                <span class="sr-only">Previous</span>
		            </a>
		        </li>
                <?php
                    for($i = 1; $i <= $page; $i++ ){
                        if($curentPage == $i){
                            echo "<li class='page-item'><a class='page-link text-danger' href='search.php?search={$search}&page={$i}'>{$i}</a></li>";
                        }
                        else{
                            echo "<li class='page-item'><a class='page-link' href='search.php?search={$search}&page={$i}'>{$i}</a></li>";
                        }
                    }
                ?>
		        <li class="page-item">
                    <a class="page-link" href="<?php if($curentPage < $page) echo "search.php?search={$search}&page=" . ($curentPage + 1);?>" aria-label="Next">
		                <span aria-hidden="true">&raquo;</span>
		                <span class="sr-only">Next</span>
		            </a>
		        </li>
		    </ul>
		</nav>
	</div>
</div>
<?php include 'footer.php'; ?>	