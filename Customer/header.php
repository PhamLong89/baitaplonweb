<?php
    ini_set('session.use_only_cookies', true);
    session_start();
?> 
<!DOCTYPE html>
<html>
    <head>
        <title>PL Fashion</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- <link rel="stylesheet" type="text/css" href="slick/slick.css"/> -->
        <!-- <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/> -->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
        <script src="../JS/jquery-3.3.1.min.js"></script>
        <script src="../JS/popper.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <!-- <script type="text/javascript" src="slick/slick.min.js"></script> -->
        <script type="text/javascript" src="script2.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    </head>
    <body>

        <div class="container-fluid sticky-top p-0">
            <div class="container-fluid bg-dark p-0 m-0" style="height: 24px;">
                <div class="col-12">
                    <div class="mx-3 float-right">
                        <a class="text-white" href="cart.php"><i class="far fa-shopping-bag text-white"></i>Giỏ hàng</a>
                        <?php 
                            if(!isset($_SESSION['cart'])){
                                echo "<span class='text-white'>(0)</span>";
                            } else{
                                $quantity = 0;
                                $cart = $_SESSION['cart'];
                                if(count($cart) > 0){
                                    foreach ($cart as $key => $value) {
                                        $quantity += $value['Quantity'];
                                    }
                                    echo "<span class='link-active'>({$quantity})</span>";
                                }
                                else{
                                    echo "<span class='text-white'>(0)</span>";
                                }
                            } 
                        ?>
                    </div>

                    <div class="mx-3 float-right dropdown">
                        <a class="text-white dropdown-toggle" href="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-user text-white"></i>
                            <?php 
                                if(!isset($_SESSION['UserID'])){
                                    echo "Tài khoản";
                                } else{
                                    echo $_SESSION['UserName'];
                                } 
                            ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <?php 
                                if(!isset($_SESSION['UserID'])){
                                    echo "<a class='dropdown-item' href='login.php'>Đăng nhập</a>";
                                    echo "<a class='dropdown-item' href='register.php'>Đăng ký</a>";
                                }
                                else{
                                    echo "<a class='dropdown-item' href='editUser.php'>Tài khoản của tôi</a>";
                                    echo "<a class='dropdown-item' href='viewOrdersCustomer.php'>Danh sách đơn hàng</a>";
                                    echo "<a class='dropdown-item' href='../Admin/logout.php'>Đăng xuất</a>";
                                }
                            ?>                           
                        </div>
                    </div>
                </div>
            </div>

            <!-- menu -->
            <nav class="container-fluid navbar navbar-expand-lg navbar-light bg-light header">
                <div class="container col-lg-8">
                    <a class="navbar-brand" href="index.php"><img style="width: 80px;" src="img/logo.jpg" alt="PhamLong" /></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown px-3">
                                <a class="nav-link" href="listProducts.php?category=Nữ" id="navbarDropdown">
                                    NỮ
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="listProducts.php?category=Áo nữ">Áo</a>
                                    <a class="dropdown-item" href="listProducts.php?category=Quần nữ">Quần</a>
                                    <a class="dropdown-item" href="listProducts.php?category=Váy nữ">Váy</a>
                                    <a class="dropdown-item" href="listProducts.php?category=Phụ kiện nữ">Phụ kiện</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown px-3">
                                <a class="nav-link" href="listProducts.php?category=Nam" id="navbarDropdown">
                                    NAM
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="listProducts.php?category=Áo nam">Áo</a>
                                    <a class="dropdown-item" href="listProducts.php?category=Quần nam">Quần</a>
                                    <a class="dropdown-item" href="listProducts.php?category=Phụ kiện nam">Phụ kiện</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown px-3">
                                <a class="nav-link" href="listProducts.php?category=Bé gái" id="navbarDropdown">
                                    BÉ GÁI
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="listProducts.php?category=Áo bé gái">Áo</a>
                                    <a class="dropdown-item" href="listProducts.php?category=Quần bé gái">Quần</a>
                                    <a class="dropdown-item" href="listProducts.php?category=Váy bé gái">Váy</a>
                                    <a class="dropdown-item" href="listProducts.php?category=Phụ kiện bé gái">Phụ kiện</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown px-3">
                                <a class="nav-link" href="listProducts.php?category=Bé trai" id="navbarDropdown">
                                    BÉ TRAI
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="listProducts.php?category=Áo bé trai">Áo</a>
                                    <a class="dropdown-item" href="listProducts.php?category=Quần bé trai">Quần</a>
                                    <a class="dropdown-item" href="listProducts.php?category=Phụ kiện bé trai">Phụ kiện</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown px-3">
                                <a class="nav-link link-active" href="#" id="navbarDropdown">
                                    MỚI
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">NỮ</a>
                                    <a class="dropdown-item" href="#">NAM</a>
                                    <a class="dropdown-item" href="#">BÉ GÁI</a>
                                    <a class="dropdown-item" href="#">BÉ TRAI</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown px-3">
                                <a class="nav-link link-active" href="#" id="navbarDropdown">
                                    GIẢM GIÁ
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">NỮ</a>
                                    <a class="dropdown-item" href="#">NAM</a>
                                    <a class="dropdown-item" href="#">BÉ GÁI</a>
                                    <a class="dropdown-item" href="#">BÉ TRAI</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <form action="search.php" method="get" class="input-group col-lg-4 p-0 pl-md-3" enctype="multipart/form-data">
                    <input type="text" class="form-control" name="search" placeholder="Tìm kiếm..." />
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </form>
            </nav>
        </div>
        
        <!-- end menu -->

        <!-- slide top -->
        <div id="carouselExampleSlidesOnly" class="carousel slide m-0 bg-dark" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <p class="text-center text-light bg-dark m-0" style="font-size: 2vw !important;">Thêm vào giỏ hàng 499.000đ để được miễn phí vận chuyển</p>
                </div>
                <div class="carousel-item">
                    <p class="text-center text-light bg-dark m-0" style="font-size: 2vw !important;">ĐỔI HÀNG MIỄN PHÍ - Tại tất cả các cửa hàng trong 15 ngày</p>
                </div>
            </div>
        </div>           