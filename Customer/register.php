<?php
    include ('../Admin/mysqliConnect.php');
    include("function.php");
?>
<?php
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $errors = array();
        if(empty($_POST['nameUser'])){
        $errors[] = "nameUser";
        }else{
            $nameUser = $conn->real_escape_string(strip_tags($_POST['nameUser']));
            if(trim($nameUser) == ""){
                $errors[] =  "nameUser";
            }
        }
    
        if(empty($_POST['phoneUser'])){
            $errors[] = "phoneUser";
        }
        else{
            $conn->real_escape_string(trim($_POST['phoneUser']));
            if(preg_match('/^[0]{1}[0-9]{9}$/', trim($_POST['phoneUser'])) || preg_match('/^[+]{1}[8]{1}[4]{1}[0-9]{9}$/', trim($_POST['phoneUser']))) {
                $phoneUser = $conn->real_escape_string(trim($_POST['phoneUser']));
            } else {
                $errors[] = "phoneUser2";
            }
        }

        if(empty($_POST['addressUser'])){
            $errors[] = "addressUser";
        }else{
            $addressUser = $conn->real_escape_string(strip_tags($_POST['addressUser']));
            if(trim($addressUser) == ""){
                $errors[] = "addressUser";
            }
        }

        if(empty($_POST['emailUser'])){
            $errors[] = 'emailUser';
        }
        else{
          if(preg_match('/^\w+@[a-zA-Z]{3,}\.com$/i', trim($_POST['emailUser']))) {
                $emailUser = $conn->real_escape_string(strip_tags($_POST['emailUser']));
            } else {
                $errors[] = "emailUser";
            }
        }
        if(preg_match('/^[\w\'.-]{4,20}$/', trim($_POST['passwordUser']))) {
            if($_POST['passwordUser'] == $_POST['confirmPassword']) {
                $passwordUser = $conn->real_escape_string(trim($_POST['passwordUser']));
            } else {
                // Neu mat khau khong phu hop voi nhau
                $errors[] = "confirmPassword";
            }
        } else {
            $errors[] = 'passwordUser';
        }

        if(empty($errors)) {
            $sql = "SELECT UserID FROM Users WHERE Email = ?";
            if($stmt = $conn->prepare($sql)) {
                $stmt->bind_param('s', $emailUser);
                $stmt->execute();           
                $result = $stmt->get_result();
                $stmt->close();
                if($result->num_rows == 0){
                    $stmt2 = $conn->prepare("INSERT INTO Users (UserName, NumberPhone, Address, Email,  PassWord) VALUES (?, ?, ?, ?, ?)");
                    $stmt2->bind_param("sssss", $nameUser, $phoneUser, $addressUser, $emailUser, sha1($passwordUser));
                    $stmt2->execute();
                    $stmt2->store_result();
                    if($stmt2->affected_rows == 1) {
                        echo"<script type='text/javascript'>
                                alert('Đăng ký tài khoản thành công');
                                window.location='http://localhost/BaiTapLonWeb/Customer/login.php';
                            </script>";
                    }
                    else{
                        $message = "<p class='error2'>Đăng kí tài khoản thất bại</p>";
                    }
                    $stmt2->close();
                    $conn->close();
                }
                else{
                    $message = "<p class='error2'>Email đã được sử dụng</p>";
                }       
            }          
        }        
    }
?>

<?php include 'header.php'; ?>

	<div class="container-fluid mt-5">
		<div class="row">
			<div class="col-md-6 text-uppercase text-center my-auto register" >
				Tạo tài khoản
			</div>
			<div class="col-md-6 bg-info p-md-4">
				<?php
					if (isset($message)) {
                        echo $message;
                    } 
                ?>
				<form class="m-md-4" action="" method="post">
					<div class="form-group">
                        <label for="nameUser" class="font-weight-bold">Họ tên<span class="text-danger">*</span>
                    	<?php
                          	if(isset($errors) && in_array('nameUser', $errors)){
                            	echo "<p class='error'>Vui lòng nhập họ và tên</p>";
                          	}
                        ?>
                        </label>
                        <input type="text" class="form-control" id="nameUser" name="nameUser"  placeholder="Họ tên" value="<?php if(isset($_POST['nameUser'])) echo strip_tags($_POST['nameUser']); ?>" required/>
                    </div>
                    <div class="form-group">
                        <label for="phoneUser" class="font-weight-bold">Số điện thoại<span class="text-danger">*</span>
                        <?php
                          	if(isset($errors) && in_array('phoneUser', $errors)){
                            	echo "<p class='error'>Vui lòng nhập số điện thoại</p>";
                          	}
                          	elseif(isset($errors) && in_array('phoneUser2', $errors)){
								echo "<p class='error'>Số điện thoại không đúng định dạng</p>";
                          	}
                        ?>
                        </label>
                        <input type="tel" class="form-control" id="phoneUser" name="phoneUser"  placeholder="0987654321" value="<?php if(isset($_POST['phoneUser'])) echo strip_tags($_POST['phoneUser']); ?>" required/>
                    </div>
                    <div class="form-group">
                        <label for="addressUser" class="font-weight-bold">Địa chỉ<span class="text-danger">*</span>
                        <?php
                          	if(isset($errors) && in_array('addressUser', $errors)){
                            	echo "<p class='error'>Vui lòng nhập địa chỉ</p>";
                          	}
                        ?>
                        </label>
                        <input type="text" class="form-control" id="addressUser" name="addressUser"  placeholder="Địa chỉ" value="<?php if(isset($_POST['addressUser'])) echo strip_tags($_POST['addressUser']); ?>" required/>
                    </div>
					<div class="form-group">
					    <label for="emailUser" class="font-weight-bold">Email <span class="text-danger">*</span>
					    <?php
                            if(isset($errors) && in_array('emailUser', $errors)){
                                echo "<p class='error'>Địa chỉ email không hợp lệ</p>";
                            }
                        ?>
                        </label>
					    <input type="email" class="form-control" id="emailUser" name="emailUser" aria-describedby="emailHelp" placeholder="Email" value="<?php if(isset($_POST['emailUser'])) echo strip_tags($_POST['emailUser']); ?>" required/>
					</div>
					<div class="form-group">
					    <label for="passwordUser" class="font-weight-bold">Mật khẩu <span class="text-danger">*</span>
				    	<?php
                          	if(isset($errors) && in_array('passwordUser', $errors)){
                            	echo "<p class='error'>Mật khẩu dài hơn 4 kí tự</p>";
                          	}
                        ?>
					    </label>
					    <input type="password" class="form-control" id="passwordUser" name="passwordUser" placeholder="Mật khẩu" value="<?php if(isset($_POST['passwordUser'])) echo strip_tags($_POST['passwordUser']); ?>"/ required>
					</div>
					<div class="form-group">
					    <label for="confirmPassword" class="font-weight-bold">Nhập lại mật khẩu <span class="text-danger">*</span>
					    <?php
                          	if(isset($errors) && in_array('confirmPassword', $errors)){
                            	echo "<p class='error'>Mật khẩu không trùng khớp</p>";
                          	}
                        ?>
                    	</label>
					    <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Nhập lại mật khẩu" value="<?php if(isset($_POST['confirmPassword'])) echo strip_tags($_POST['confirmPassword']); ?>" required/>
					</div>
					<button type="submit" class="btn btn-dark text-white my-3 my-md-5">Đăng ký</button>
				</form>
			</div>
		</div>
	</div>

<?php include 'footer.php'; ?>	