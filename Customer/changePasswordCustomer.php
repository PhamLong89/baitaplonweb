<?php
    include ('../Admin/mysqliConnect.php');
    include("function.php");
?>
<?php
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        $errors = array();        
        if(isset($_POST['currentPassword']) && preg_match('/^\w{4,20}$/', trim($_POST['currentPassword']))) {
            $currentPassword = $conn->real_escape_string(trim($_POST['currentPassword']));               
            $sql = "SELECT UserName FROM Users WHERE UserID = ? AND PassWord = ?";
	        if($stmt = $conn->prepare($sql)) {

	            $stmt->bind_param('is', $_SESSION['UserID'], sha1($currentPassword));
	            $stmt->execute();           
	            $result = $stmt->get_result();

	            if($result->num_rows == 1){

	                if(isset($_POST['newPassword']) && preg_match('/^\w{4,20}$/', trim($_POST['newPassword']))) {

                        if($_POST['newPassword'] == $_POST['confirmPassword']) {
                            $newPassword = $conn->real_escape_string(trim($_POST['newPassword'])); 
                            $sql = "UPDATE Users SET PassWord = ? WHERE UserID = ? LIMIT 1";
                            if($upStmt = $conn->prepare($sql)) {
			                    $upStmt->bind_param('si', sha1($newPassword), $_SESSION['UserID']);
			                    $upStmt->execute();
			                    if($upStmt->affected_rows == 1) {
			                        $message = "<p class='success'>Đổi mật khẩu thành công</p>";
			                    }
			                    else{
			                        $message = "<p class='error2'>Đổi mật khẩu thất bại</p>";
			                    }           
			                }                           
                        } else {
                            $message = "<p class='error2'>Mật khẩu không trùng khớp</p>";
                        }
	                    
                    } else {
                        $message = "<p class='error2'>Mật khẩu mới phải nhiều hơn 4 kí tự</p>";
                    }
	                    
	            } else {
	                $message = "<p class='error2'>Mật khẩu hiện tại không chính xác</p>";
	            }
	        }
        } else {
           	$message = "<p class='error2'>Mật khẩu hiện tại không chính xác</p>";
        }
    } // END main IF
?>
<?php include 'header.php'; ?>
<div class="container mt-5">
	<div class="row">
        <div class="col-md-3 my-auto ">
            <a class="row my-2" href="editUser.php">Thông tin tài khoản</a>
            <a class="row my-2" href="viewOrdersCustomer.php">Danh sách đơn hàng</a>
            <a class="row my-2 link-active" href="changePasswordCustomer.php">Đổi mật khẩu</a>
            <a class="row my-2" href="../Admin/logout.php">Đăng xuất</a>
        </div>
        <div class="col-md-9">
		    <h4 class="text-uppercase font-weight-bold title-cart">Đổi mật khẩu</h4>
		    <hr />
		    <?php if (isset($message)) {
		        echo $message;
		    } ?>
		    <form class="m-md-4" action="" method="post">
				<div class="form-group">
				    <label for="currentPassword" class="font-weight-bold">Mật khẩu hiện tại <span class="text-danger">*</span></label>
				    <input type="password" class="form-control" id="currentPassword" placeholder="Mật khẩu hiện tại" name="currentPassword"/>
				</div>
				<div class="form-group">
				    <label for="newPassword" class="font-weight-bold">Mật khẩu mới <span class="text-danger">*</span></label>
				    <input type="password" class="form-control" id="newPassword" placeholder="Mật khẩu mới" name="newPassword"/>
				</div>
				<div class="form-group">
				    <label for="confirmPassword" class="font-weight-bold">Nhập lại mật khẩu <span class="text-danger">*</span></label>
				    <input type="password" class="form-control" id="confirmPassword" placeholder="Nhập lại mật khẩu" name="confirmPassword"/>
				</div>

				<button type="submit" class="btn btn-info text-white my-3 mt-md-5">Đổi mật khẩu</button>
				<button type="button" class="btn btn-dark my-3 mt-md-5"><a class="text-light" href="admin.php">Hủy</a></button>
			</form>
		</div>
	</div>
</div>
<?php include 'footer.php'; ?>  