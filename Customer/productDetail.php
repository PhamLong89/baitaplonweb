<?php include '../Admin/mysqliConnect.php'; ?>
<?php include 'function.php'; ?>
<?php 
    if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT, array('min_range' =>1))) {
        $productId = $_GET['id'];
        //Lấy sản phẩm có id cần xem
        $sql = "SELECT ProductName, ProductPrice, Sale, CategoryName, Description FROM Products WHERE ProductId = ?";
        if($stmt = $conn->prepare($sql)) {
            $stmt->bind_param('i', $productId);
            $stmt->execute();           
            $result = $stmt->get_result();
            if($result->num_rows == 1){
                $product = $result->fetch_assoc();
                //Lấy các variant tương ứng của sản phẩm
                $listVariant = array();
                $sqlVariant = "SELECT Color, Size, SKU, Quantity, Image1, Image2 FROM Variants WHERE ProductId = ?";
                if($stmt2 = $conn->prepare($sqlVariant)) {
                    $stmt2->bind_param('i', $productId);
                    $stmt2->execute();          
                    $result2 = $stmt2->get_result();
                    while($variant = $result2->fetch_assoc()) {
                        $listVariant[] = $variant;   
                    }
                }

                //Lấy color của sản phẩm
                $listColor = array();
                $sqlVariant = "SELECT DISTINCT Color FROM Variants WHERE ProductId = ?";
                if($stmt2 = $conn->prepare($sqlVariant)) {
                    $stmt2->bind_param('i', $productId);
                    $stmt2->execute();          
                    $result2 = $stmt2->get_result();
                    while($color = $result2->fetch_assoc()) {
                        $listColor[] = $color;   
                    }
                }

                //Lấy id các sản phẩm tương tự
                $listProducts = array();
                $sql3 = "SELECT ProductId FROM Products WHERE CategoryName = ? AND ProductId != ?";
                $stmt3 = $conn->prepare($sql3);
                $stmt3->bind_param('si', $product['CategoryName'], $productId);
                $stmt3->execute();          
                $result3 = $stmt3->get_result();
                while($row1 = $result3->fetch_assoc()) {
                    //Lấy ảnh và thông tin của các sản phẩm tương tự
                    $sql = "SELECT Variants.ProductId, ProductName, ProductPrice, Sale, Image1, Image2 FROM Variants INNER JOIN Products ON Variants.ProductId = Products.ProductId WHERE Variants.ProductId = ? AND (Image1 IS NOT NULL OR Image2 IS NOT NULL) LIMIT 1";
                    if($stmt = $conn->prepare($sql)) {
                        $stmt->bind_param('i', $row1['ProductId']);
                        $stmt->execute();           
                        $result4 = $stmt->get_result();
                        if($result4->num_rows == 1){
                            $listProducts[] = $result4->fetch_assoc();
                        }
                    }
                }
            }
            else{
                redirect_to('Customer/index.php');
            }
        } 

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $errors = array();
            if(isset($_POST['color']) && filter_var($_POST['color'], FILTER_SANITIZE_STRING)) {
                $color = $_POST['color'];
                if(trim($color) == ""){
                    $errors[] = "color";
                }
            }
            else {
                $errors[] = "color";
            }
            $size = $_POST['size'];
            if(trim($size) === "null"){
                $errors[] = "size";
            }
            if(empty($_POST['SKU'])){
                $errors[] = "SKU";
            }
            else{
                $SKU = $conn->real_escape_string(trim($_POST['SKU']));          
            }
            $quantityRepository = 10000;
            foreach ($listVariant as $key => $value) {
                if($value['SKU'] == $SKU){
                    $quantityRepository = $value['Quantity'];
                    break;
                }
            }
            if(empty($_POST['quantityDetail'])){
                $errors[] = "quantityDetail";
            }
            else{
                if(!filter_var($_POST['quantityDetail'], FILTER_VALIDATE_INT)){
                    $errors[] = "quantityDetail";
                }
                else{
                    $quantityDetail = $_POST['quantityDetail'];
                    if($quantityDetail < 1){
                        $errors[] = "quantityDetail";
                    }
                    else if($quantityDetail > $quantityRepository){
                        $errors[] = "quantityDetail";
                        echo"<script>
                                alert('Số lượng yêu cầu không thể đáp ứng. Vui lòng chọn lại số lượng.');
                            </script>";
                    }
                }
            }
            $price = $_POST['price'];
            $name = $_POST['name'];
            $sale = $_POST['sale'];

            if(empty($errors)) {
                session_regenerate_id();
                //Lấy ảnh sản phẩm
                foreach ($listVariant as $key => $value) {
                    if($value['Image1'] != null){
                        $image = $value['Image1'];
                        break;
                    }
                    if($value['Image2'] != null){
                        $image = $value['Image2'];
                        break;
                    }
                }
                //Nếu chưa có giỏ hàng
                if(!isset($_SESSION['cart'])){
                    $cart = array();
                    $item = array('SKU' => $SKU, 'ProductName' => $name, 'Color' => $color, 'Size' => $size, 'Quantity' => $quantityDetail, 'Price' => $price, 'Sale' => $sale, 'Image' => $image);
                    $cart[] = $item;
                    $_SESSION['cart'] = $cart;
                }
                else{
                    $productExist = false;
                    $cart = $_SESSION['cart'];
                    foreach ($cart as $key => $item) {
                        //Nếu đã có sản phẩm trong giỏ hàng thì tăng số lượng
                        if($item['SKU'] == $SKU){
                            $cart[$key]['Quantity'] = $item['Quantity'] + $quantityDetail;
                            $productExist = true;
                            break;
                        }
                    }
                    //Nếu chưa có sản phẩm trong giỏ hàng thì thêm vào
                    if(!$productExist){
                        $item = array('SKU' => $SKU, 'ProductName' => $name, 'Color' => $color, 'Size' => $size, 'Quantity' => $quantityDetail, 'Price' => $price, 'Sale' => $sale, 'Image' => $image);
                        $cart[] = $item;
                    }
                    $_SESSION['cart'] = $cart;
                }
                echo"<script>
                        alert('Thêm sản phẩm vào giỏ hàng thành công');
                    </script>";
            }
        }

    }
    else{
        redirect_to('Customer/index.php');
    }

?>
<?php include 'header.php'; ?>
<div class="container-fluid">
    <div class="row mt-5">
        <!-- Hiển thị ảnh sản phẩm -->
        <div class="col-md-6 col-12 pl-md-5">
            <div class="slider-for row">
                <?php 
                    foreach ($listVariant as $key => $variant) {
                        if($variant['Image1'] != null){
                            echo "<div class=''><img src=\"../Admin/img/{$variant['Image1']}\" alt='' style='width: 100%; height: auto;' /></div>";
                        }
                        if($variant['Image2'] != null){
                            echo "<div class=''><img src=\"../Admin/img/{$variant['Image2']}\" alt='' style='width: 100%; height: auto;' /></div>";
                        }
                    }
                ?>
            </div>

            <div class="slider-nav mt-4 mx-0 row">
                <?php 
                    foreach ($listVariant as $key => $variant) {
                        if($variant['Image1'] != null){
                            echo "<div class=''><img src=\"../Admin/img/{$variant['Image1']}\" alt='' style='width: 97%; height: auto;' /></div>";
                        }
                        if($variant['Image2'] != null){
                            echo "<div class=''><img src=\"../Admin/img/{$variant['Image2']}\" alt='' style='width: 97%; height: auto;' /></div>";
                        }
                    }
                ?>
            </div>
        </div>
        <!-- Hiển thị thông tin chi tiết sản phẩm -->
        <div class="col-md-6 col-12 pl-md-5">
            <form action="" method="POST" class="" enctype="multipart/form-data">
                <h4 class="text-uppercase font-weight-bold name-product mt-4"><?php echo $product['ProductName']; ?></h4>
                <input type="hidden" id="name" name="name" value="<?php echo $product['ProductName'] ?>">
                <div class="form-group m-0">
                    <label class="font-weight-bold">Mã: <?php echo $productId ?></label>
                    <input type="hidden" id="productId" name="productId" value="<?php echo $productId ?>">
                </div>
                <div class="form-group m-0">
                    <label class="font-weight-bold" id="labelSKU">SKU: </label>
                    <input type="hidden" id="SKU" name="SKU" value="">
                </div>
                <p class="font-weight-bold <?php if($product['Sale'] > 0) echo "sale-price"; else echo 'link-active'; ?>" ><?php echo $product['ProductPrice'] . " đ";?></p>
                <?php 
                    if($product['Sale'] > 0){
                        $newPrice = $product['ProductPrice'] - round(($product['ProductPrice'] * $product['Sale']/100));
                        echo "<p class='font-weight-bold link-active' >{$newPrice} đ</p>";
                        echo "<p class='link-active' >Giảm: {$product['Sale']}%</p>";
                    } 
                ?>
                <input type="hidden" id="price" name="price" value="<?php echo $product['ProductPrice'];?>">
                <input type="hidden" id="sale" name="sale" value="<?php echo $product['Sale'];?>">
                <div class="row">
                    <div class="form-group col-6">
                        <label class="font-weight-bold" for="">Chọn màu <span class="text-danger">*</span></label>
                        <select class="form-control" id="color" name="color" required>
                            <?php 
                                foreach ($listColor as $key => $color) {
                                    if(isset($_POST['color']) && $color['Color'] == $_POST['color']){
                                        echo "<option value={$color['Color']} selected>{$color['Color']}</option>";
                                    }
                                    else{
                                        echo "<option value={$color['Color']}>{$color['Color']}</option>";
                                    }
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-6">
                        <label class="font-weight-bold" for="">Chọn kích cỡ <span class="text-danger">*</span>
                        </label>
                        <select class="form-control" id="size" name="size" required>
                            <option value="null">Chọn kích cỡ</option>
                        </select>
                        <?php
                            if(isset($errors) && in_array('size', $errors)){
                                echo "<p class='error float-left mt-3'>Vui lòng chọn kích cỡ</p>";
                            }
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="d-block font-weight-bold" for="">Chọn số lượng <span class="text-danger">*</span>
                    <?php
                        if(isset($errors) && in_array('quantityDetail', $errors)){
                            echo "<p class='error'>Vui lòng chọn số lượng</p>";
                        }
                    ?>
                    </label>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-secondary quantity-minus" onclick="quantityMinus($('#quantityDetail'))"><i class="fas fa-minus"></i></button>
                        <input class="text-center quantity" id="quantityDetail" type="number" name="quantityDetail" value="<?php if(isset($_POST['quantityDetail'])) echo strip_tags($_POST['quantityDetail']); else echo 1; ?>" min="1" onkeypress="isInputNumber(event)" required/>
                        <button type="button" class="btn btn-secondary quantity-plus" onclick="quantityPlus($('#quantityDetail'))"><i class="fas fa-plus"></i></button>
                    </div>
                </div>

                <button type="submit" class="btn btn-danger text-uppercase font-weight-bold mt-3">Thêm vào giỏ hàng</button>
                <div class="form-group mt-4">
                    <label class="font-weight-bold" for="">Mô tả</label>
                    <p>
                        <?php echo $product['Description']?>
                    </p>
                </div>
            </form>
        </div>
    </div>

    <div class="row mt-3">
        <hr class="clearfix w-100" />
        <h4 class="text-uppercase font-weight-bold name-product ml-3">Sản phẩm tương tự</h4>
    </div>
</div>

<div class="container-fluid">
    <div class="autoplay">
        <!-- Hiển thị sản phẩm tương tự -->
        <?php 
            foreach ($listProducts as $key => $pro) {
                if($pro['Sale'] == 0){
                    echo"
                        <div class='col-md-12 col-6 p-1 p-md-3'>
                            <div class='card'>
                                <a href=\"productDetail.php?id={$pro['ProductId']}\"><img class='card-img-top p-md-3' src=\"../Admin/img/";
                    if($pro['Image1'] != null){
                        echo $pro['Image1'];
                    }
                    else{
                        echo $pro['Image2'];
                    }
                    echo "\" alt=\"{$pro['ProductName']}\" /></a>
                                <div class='card-body'>
                                    <p class='card-title text-center text-truncate'><a href=\"productDetail.php?id={$pro['ProductId']}\" class='text-decoration-none'>{$pro['ProductName']}</a></p>
                                    <h5 class='card-text font-weight-bold text-center'>{$pro['ProductPrice']} đ</h5>
                                </div>
                            </div>
                        </div>
                        ";
                }
                else{
                    echo"
                        <div class='col-md-12 col-6 p-1 p-md-3'>
                            <div class='card'>
                                <div class='ribbon-wrapper'>
                                    <div class='ribbon'>Giảm {$pro['Sale']}%</div>
                                </div>
                                <a href=\"productDetail.php?id={$pro['ProductId']}\"><img class='card-img-top p-md-3' src=\"../Admin/img/";
                    if($pro['Image1'] != null){
                        echo $pro['Image1'];
                    }
                    else{
                        echo $pro['Image2'];
                    }
                    echo "\" alt=\"{$pro['ProductName']}\" /></a>
                                <div class='card-body'>
                                    <p class='card-title text-center text-truncate'><a href=\"productDetail.php?id={$pro['ProductId']}\" class='text-decoration-none'>{$pro['ProductName']}</a></p>
                                    <h5 class='card-text font-weight-bold text-center'>{$pro['ProductPrice']} đ</h5>
                                </div>
                            </div>
                        </div>
                        ";
                }
            }
        ?>
    </div>
</div>


<?php include 'footer.php'; ?>
