<?php
include 'sidebarAdmin.php';
include 'mysqliConnect.php';
?>

			<main class="page-content">
                <div class="container-fluid">
                    <h4 class="text-uppercase text-danger">Đổi mật khẩu</h4>
                    <hr />
                    <?php if (isset($message)) {
                        echo $message;
                    } ?>
                    <form action="" method="POST">
                    	<div class="form-group">
						    <label for="curentPassword" class="font-weight-bold">Mật khẩu cũ<span class="text-danger">*</span></label>
						    <input type="password" class="form-control" id="curentPassword" placeholder="Mật khẩu cũ" />
						</div>
                    	<div class="form-group">
						    <label for="passwordNew" class="font-weight-bold">Mật khẩu mới<span class="text-danger">*</span></label>
						    <input type="password" class="form-control" id="passwordNew" placeholder="Mật khẩu mới" />
						</div>
						<div class="form-group">
						    <label for="confirmPassword" class="font-weight-bold">Nhập lại mật khẩu <span class="text-danger">*</span></label>
						    <input type="password" class="form-control" id="confirmPassword" placeholder="Nhập lại mật khẩu" />
						</div>
                    	<button type="submit" class="btn btn-info mt-4">Đổi mật khẩu</button>
                    </form>
                </div>
            </main>
            <!-- page-content" -->
        </div>
    </body>
</html>